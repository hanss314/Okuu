# Instructions
|OPCODE | ASM | ARGS | Description 
--------------------------
|   0   | NOP |  0   | Does nothing
|   1   | MOV |  4 4 | Copy value from register/bus to register/bus
|   2   | JMP |  Any | Jump to instruction
|   3   | JCD |  Any | Jump if conditional flag is true
   
|   4   | ARI |  4 4 | Perform arithmetic. 
                       First specifies operation, 
                       second specifies register.
                       Operation performed on acc and 
                       result stored in acc
   
|   9   | TEQ |      | sets conditional flag to register == acc
|   A   | TGT |      | sets conditional flag to register > acc
|   B   | TLT |      | sets conditional flag to register < acc
   
|   C   | TRU |      | sets conditional flag to true
|   D   | FLS |      | sets conditional flag to false
|   E   | NOT |      | invert conditional flag
   
|   F   | END |      | Terminate program

----

# Registers
|HEX|Name | Description
------------------
| 0 | nul | Null register. writing does nothing, reading produces 0
| 1 | acc | Main arithmetic register. arithmetic operations performed on this
| 2 | rg0 | Data register, free r/w
| 3 | rg1 | See above
| 4 | rg2 | See above
| 5 | rg3 | See above

# Ports
|HEX|Name | Description
------------------
| 7 | dp0 | position of dsk internal pointer (see `dpt`), first 6 bits always treated as 0
| 8 | dsk | r/w the value stored at r/w head. r/w increments head
| 9 | dpt | Position of r/w head. Contains an internal pointer with vals 0,1,2,3. 
|   |     | Each r/w to/from dpt increments the internal pointer.
|   |     | sets/returns 8 bits from bits 8\*i to 8\*i+7 of the r/w head 
|   |     | where i is the internal pointer

| A | mp0 | position of internal ptr (see `mpt`), first 7 bits always treated as 0
| B | mem | r/w from memory
| C | mpt | Sets mem ptr. Contains internal pointer with als 0,1
|   |     | See docs for `dpt`

| D | iop | Read byte from input stream or write byte to output.
|   |     | Output is printed on newline and only on newline

| E | nuk | launches nuke

# Special
| F |     | Read next 8 bits as an 8 bit number
