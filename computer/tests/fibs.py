import opc
opc.set_diskptr(10)

opc.write_disk(0b00011110)
opc.write_disk(0)
opc.write_disk(0)

opc.write_disk(0b01010001)

opc.write_disk(0b11100000)
opc.write_disk(0)
opc.write_disk(0b00010010)

opc.write_disk(0b00010101)
opc.write_disk(0b00010001)
opc.write_disk(0b00011101)

opc.write_disk(0b01000000)
opc.write_disk(0b00100001)
opc.write_disk(0b00010011)

opc.write_disk(0b00010010)
opc.write_disk(0b00010001)
opc.write_disk(0b00110010)

opc.write_disk(0b10101110)
opc.write_disk(1)
opc.write_disk(0)

opc.write_disk(0b00010001)
opc.write_disk(0b01010001)
opc.write_disk(0b11100000)
opc.write_disk(0)
opc.write_disk(0b11100001)
opc.write_disk(0b00111111)


opc.run_address(10, "")

print(opc.get_output())
