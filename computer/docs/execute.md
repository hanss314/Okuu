To execute the code at a certain location in disk, use `{pfx}opc run <location> [args]`

Passed args can be accessed by the program one byte at a time by reading `iop`. 
At the end of execution, whatever has been written to `iop` port will be \
returned to the user. 
If `iop` consists solely of non printable characters, \
OPC will return "There was no output" instead.

OPC is currently a single process machine, only one process may run at a time. 
This will likely change. For now, there is a 2\*\*16 instruction limit for each process.
After each process has executed 2\*\*16 instructions, it will automatically exit.

During execution all `jmp` instructions will be zeroed at the `location` parameter that was passed.

See `{pfx}opc docs instructions` for more information on the instruction set.
See `{pfx}opc docs registers iop` for more information on the `iop` register.
