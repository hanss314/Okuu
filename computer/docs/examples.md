**Please read documentation to understand the examples**
```
mov 10, iop 
end
```
Hex: `1e000adf`
Bin: `0001 1110 0000 0000 0000 1010 1101 1111`
Breakdown:
```
Hex:    1 
Bin:   0001 
mov, the instruction  

Hex:   e    0    0    0    a 
Bin:  1110 0000 0000 0000 1010 i
4 nibble number, 10 in this case

Hex:   d                    f   
Bin:  1101                 1111
iop, the register   end, the instruction
```
