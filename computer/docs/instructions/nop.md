`nop` - `0` - No OPeration

Parameters: None

A `nop` instruction will always occupy one nibble. \
After a `nop` instruction, the current instruction \
pointer will increment by one nibble. \
This behavior is consistent across parameterless instructions. 
