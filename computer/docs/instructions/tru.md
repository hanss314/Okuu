`tru` - `C` - TRUe
`fls` - `D` - FaLSe
`not` - `E` - NOT 

Parameters: None

Sets the flag to true or false accordingly. 
`not` sets the flag to true if it is false, and to false if it is true.

For information on the flag, use `{pfx}opc docs flag` 
