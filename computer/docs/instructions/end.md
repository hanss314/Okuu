`end` - `F` - END

Parameters: None

Terminates execution of the program. 
Whatever is in the output buffer is flushed and sent as a message.
