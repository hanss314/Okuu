`jmp` - `2` - JuMP
`jcd` - `3` - Jump ConDitionally

Parameters: None

`jmp` will set the instruction pointer to the value \
currently stored in the `acc` register. 
0 will refers to location on disk that the current process \
was called from. 1 refers to 1 nibble after that point, \
2 refers to 2 nibbles after that point etc. 
Therefore, it is possible to run what would normally \
be interpreted as a register, nibble or integer as \
an instruction.

`jcd` will perform a `jmp` if and only if the flag is set to true.

For information on the flag, use `{pfx}opc docs flag` 
