`teq` - `9` - Test EQuality
`tgt` - `A` - Test Greater Than
`tlt` - `B` - Test Less Than

Parameters: `R`/`I`

Tests if the the value read from the parameter is \
equal to, greater than, or less than the `acc` register, \
then sets the flag accordingly.

For information on the flag, use `{pfx}opc docs flag` 
