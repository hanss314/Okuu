`ari` - `4` - ARIthmetic

Parameters: `N` `R`/`I` 

Performs arithmetic. The operation is specified by the nibble. 
The result is always written to the `acc` register. 
Let `val` refer to the value read from the register or integer.
The following is a chart of which operation is performed for each \
nibble value.

`0`: acc + val 
`1`: acc - val 
`2`: val - acc 
`3`: acc * val (multiplication) 
`4`: acc / val (integer division) 
`5`: val / acc (integer division) 
`6`: acc % val (modulo) 
`7`: val % acc (modulo) 
`8`: acc | val (bitwise or) 
`9`: acc & val (bitwise and) 
`A`: acc ^ val (bitwise xor)  
`B`, `C`, `D`, `E`: Unused (will possibly change) 
`F`: ~acc (bitwise complement, val is ignored, but still mandatory)

