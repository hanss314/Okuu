`mov` - `1` - MOVe

Parameters: `R`/`I` `R`

If the first parameter is a register, \
it will be read from and written to the second register.
If the first parameter is an integer, \
the integer will be written to the specified register.

See `{pfx}opc docs regs` for help on registers.
See `{pfx}opc docs ints` for help on integers.
