To write to the OPC's disk, use `{pfx}opc write <location> <hex or binary>`.

OPC has 4MiB bytes of disk space. 
Trying to write outside of that range will result in an error. 
OPC is a public machine, so anyone can write to the disk and \
overwrite what you have written. 
Protected ranges will probably be implemented later.
