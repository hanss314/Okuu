The flag is a value stored by the CPU. 
It can either be in a true state, or a false state. 
At the beginning of a program execution, \
the flag may be in either state. 
Use `tru` or `fls` if you require the flag to be in a \
particular state at the start of program execution.

The value of the flag affects the behavior of `jcd` and `not`.
The value of the flag can be set by `teq`, `tlt`, `tgt`, `tru`, `fls` and `not`. 

Use `{pfx}opc docs instrs` for help on instructions.
