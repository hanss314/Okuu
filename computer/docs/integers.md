Integers often be specified in place of a register. 
An `E` or `F` nibble specifies the start of an integer. 

An `E` nibble specifies that the next four nibbles (16 bits) \
should be read as a big endian integer.
An `F` nibble specifies that the next eight nibbles (32 bits) \
should be read as a big endian integer.

After reading an integer, the instruction pointer will increment \
by a proportional amount, so that the integer specifying portion \
will not be read as code on the next fetch.
