`iop` - D - IO Port

Used for i/o. 
Reading produces the next byte of the input. 
Writing appends the last 8 bits (1 byte) to the output buffer. 
At the end of program execution the output buffer is sent as a message. 

Planned features: The output buffer is sent and cleared when newline is written.
