`dsk` - `8` - DiSK
`dpt` - `9` - Disk PoinTer 

Used for disk access. OPC has 4MiB of disk space.

`dpt` stores a 22-bit integer. 
Reading produces the current disk pointer location. 
Writing sets the disk pointer to the value written. 
All bits before the last 22 bits are ignored.

`dsk` is the value at `dpt`. 
Writing will write the value to disk at the location specified by `dpt`. 
Reading will produce the value stored on the disk at `dpt`. 
Everything except for the last 8 bits (1 byte) are ignored. 
Reading and writing will increment `dpt`.

Disk storage will persist across restarts.
