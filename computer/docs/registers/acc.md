`acc` - `1` - ACCumulator

Main register utilised by instructions. 
Stores a 64 bit unsigned integer. 
Provides address for `jmp` and `jcd`. 
`ari` operates on `acc`
