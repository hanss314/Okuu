`nul` - `0` - `NULl`

Null register. Reading produces 0, writing does nothing.
