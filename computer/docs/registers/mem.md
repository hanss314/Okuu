`mem` - `B` - MEMory
`mpt` - `C` - Memory PoinTer 

Used for memory access. OPC has 64KiB of RAM.

`mpt` stores a 16-bit integer. 
Reading produces the current memory pointer location. 
Writing sets the memory pointer to the value written. 
The first 16 bits are ignored. 

`mem` is the value at `mpt`. 
Writing will write the value to memory at the location specified by `mpt`. 
Reading will produce the value stored in memory at `mpt`. 
Everything except for the last 8 bits (1 byte) are ignored.

Memory does not persist across restarts. 
