`rg0` - `2` - ReGister 0 
`rg1` - `3` - ReGister 1 
`rg2` - `4` - ReGister 2 
`rg3` - `5` - ReGister 3

Storage registers. Values are not changed implicitly by any instruction. 
`rg3` may be removed in the future, avoid use of `rg3`. 

Writing will cause the register to store the value. 
Reading will produce the previously written value. 
The registers are not guaranteed to have any particular value \
at the beginning of execution.
