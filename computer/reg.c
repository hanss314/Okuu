#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "reg.h"
#include "opc.h"

const char* reg_str[REG_COUNT] = {
    "nul",
    "acc",
    "rg0",
    "rg1",
    "rg2",
    "rg3",
    "",
    "",
    "dsk",
    "dpt",
    "",
    "mem",
    "mpt",
    "iop",
    "nuk",
};

unsigned int (*reg_read[REG_COUNT]) () = {
    &read_nul,
    &read_acc,
    &read_rg0,
    &read_rg1,
    &read_rg2,
    &read_rg3,
    &read_nul,
    &read_nul,
    &read_dsk,
    &read_dpt,
    &read_nul,
    &read_mem,
    &read_mpt,
    &read_iop,
    &read_nul,
};

void (*reg_writ[REG_COUNT]) (unsigned int) = {
    &writ_nul,
    &writ_acc,
    &writ_rg0,
    &writ_rg1,
    &writ_rg2,
    &writ_rg3,
    &writ_nul,
    &writ_nul,
    &writ_dsk,
    &writ_dpt,
    &writ_nul,
    &writ_mem,
    &writ_mpt,
    &writ_iop,
    &writ_nul,
};

unsigned int acc=0, rg0=0, rg1=0, rg2=0, rg3=0;
unsigned int memptr=0;

char* input;
char* output;
int olen=0;
int bufsize=0;
int inpos = 0;

unsigned int read_nul() { return 0; }
void writ_nul(unsigned int i){}

unsigned int read_acc(){ return acc; }
void writ_acc(unsigned int i){ acc = i; }
unsigned int read_rg0(){ return rg0; }
void writ_rg0(unsigned int i){ rg0 = i; }
unsigned int read_rg1(){ return rg1; }
void writ_rg1(unsigned int i){ rg1 = i; }
unsigned int read_rg2(){ return rg2; }
void writ_rg2(unsigned int i){ rg2 = i; }
unsigned int read_rg3(){ return rg3; }
void writ_rg3(unsigned int i){ rg3 = i; }

unsigned int read_dpt(){ return diskptr; }
void writ_dpt(unsigned int i){ diskptr = i; }
unsigned int read_dsk(){
    unsigned char c;
    if (diskptr < 0 || diskptr >= DISK){
        diskptr %= DISK;
    }
    fseek(disk, diskptr, SEEK_SET);
    fread(&c, 1, 1, disk);
    diskptr++;
    return (unsigned int)c;
}
void writ_dsk(unsigned int i){
    unsigned char c = (char) i;
    if (diskptr < 0 || diskptr >= DISK){
        diskptr %= DISK;
    }
    fseek(disk, diskptr, SEEK_SET);
    fwrite(&c, 1, 1, disk);
    diskptr++;
}


unsigned int read_mpt(){ return memptr; }
void writ_mpt(unsigned int i){ memptr = i; }
unsigned int read_mem(){
    if (memptr < 0 || memptr >= RAM) memptr %= RAM;
    return ram[memptr];
}
void writ_mem(unsigned int i){
    if (memptr < 0 || memptr >= RAM) memptr %= RAM;
    ram[memptr] = (char) i;
}

void writ_iop(unsigned int i){
    while (olen >= bufsize-4) {
        output = realloc(output, bufsize*2);
        bufsize *= 2;
    }
    output[olen] = i;
    olen += 1;
    output[olen] = '\0';
}


unsigned int read_iop(){
    char val = input[inpos];
    if (val != '\0') inpos++;
    return val;
}
