#include <Python.h>
#include <stdbool.h>
#include "opc.h"
#include "ops.h"

FILE *disk;
unsigned int diskptr = 0;
unsigned char ram[RAM];
bool flag = false;

extern char* input;
extern char* output;

void OPC_init(){
    disk = fopen(DISKFILE, "rw+b");
    output = malloc(1); output[0] = '\0';
}
void OPC_close(){
    fclose(disk);
    free(output);
}



static PyObject* OPC_set_ram(PyObject *self, PyObject *args){
    unsigned int i; unsigned char c;
    if (!PyArg_ParseTuple(args, "bi", &c, &i)) return NULL;
    if (i < 0 || i >= 65536)
        return PyErr_Format(PyExc_IndexError, "Memory index out of range");
    
    ram[i] = c;
    Py_RETURN_NONE;
}

static PyObject* OPC_get_ram(PyObject *self, PyObject *args){
    unsigned int i;
    if (!PyArg_ParseTuple(args, "i", &i)) return NULL;
    if (i < 0 || i >= RAM)
        return PyErr_Format(PyExc_IndexError, "Memory index out of range");
    
    return PyLong_FromLong(ram[i]);
}

static PyObject* OPC_set_diskptr(PyObject *self, PyObject *args){
    unsigned int i;
    if (!PyArg_ParseTuple(args, "i", &i)) return NULL;
    if (i < 0 || i >= DISK)
        return PyErr_Format(PyExc_IndexError, "Device address out of range");
    
    diskptr = i;
    Py_RETURN_NONE;
}

static PyObject* OPC_read_disk(PyObject *self, PyObject *args){
    unsigned char c;
    if (diskptr < 0 || diskptr >= DISK){
        return PyErr_Format(PyExc_IndexError, "Device address out of range");
    }
    fseek(disk, diskptr, SEEK_SET);
    fread(&c, 1, 1, disk);
    diskptr++;

    return PyLong_FromLong((unsigned int)c);
}

static PyObject* OPC_write_disk(PyObject *self, PyObject *args){
    unsigned char c;
    if (!PyArg_ParseTuple(args, "b", &c)) return NULL;
    if (diskptr < 0 || diskptr >= DISK){
        return PyErr_Format(PyExc_IndexError, "Device address out of range");
    }
    fseek(disk, diskptr, SEEK_SET);
    fwrite(&c, 1, 1, disk);
    diskptr++;

    Py_RETURN_NONE;
}

static PyObject* OPC_run_address(PyObject *self, PyObject *args){
    unsigned int i;
    if (!PyArg_ParseTuple(args, "is", &i, &input)) return NULL;
    if (i < 0 || i >= DISK)
        return PyErr_Format(PyExc_IndexError, "Device address out of range");

    run_address(i);
    Py_RETURN_NONE;

}

static PyObject* OPC_get_output(PyObject *self, PyObject *args){
    return PyBytes_FromString(output);
}

static PyMethodDef ModuleMethods[] = {
    {"set_ram", OPC_set_ram, METH_VARARGS, ""},
    {"get_ram", OPC_get_ram, METH_VARARGS, ""},
    {"set_diskptr", OPC_set_diskptr, METH_VARARGS, ""},
    {"read_disk", OPC_read_disk, METH_VARARGS, ""},
    {"write_disk", OPC_write_disk, METH_VARARGS, ""},
    {"run_address", OPC_run_address, METH_VARARGS, ""},
    {"get_output", OPC_get_output, METH_VARARGS, ""},
    {NULL, NULL, 0, NULL}
};

static struct PyModuleDef opcmodule = {
    PyModuleDef_HEAD_INIT,
    "opc",   /* name of module */
    NULL, /* module documentation, may be NULL */
    -1,
    ModuleMethods
};

PyObject *createClassObject(const char *name, PyMethodDef methods[])
{
    PyObject *pClassName = PyUnicode_FromString(name);
    PyObject *pClassBases = PyTuple_New(0); // An empty tuple for bases is equivalent to `(object,)`
    PyObject *pClassDic = PyDict_New();

    PyMethodDef *def;
    // add methods to class
    for (def = methods; def->ml_name != NULL; def++)
    {
        PyObject *func = PyCFunction_New(def, NULL);
        PyObject *method = PyInstanceMethod_New(func);
        PyDict_SetItemString(pClassDic, def->ml_name, method);
        Py_DECREF(func);
        Py_DECREF(method);
    }

    // pClass = type(pClassName, pClassBases, pClassDic)
    PyObject *pClass = PyObject_CallFunctionObjArgs((PyObject *)&PyType_Type, pClassName, pClassBases, pClassDic, NULL);

    Py_DECREF(pClassName);
    Py_DECREF(pClassBases);
    Py_DECREF(pClassDic);

    return pClass;
}

PyMODINIT_FUNC PyInit_opc(void) {
    PyObject *module;
    OPC_init();
    Py_AtExit(&OPC_close);
    module = PyModule_Create(&opcmodule);
    return module;
}
