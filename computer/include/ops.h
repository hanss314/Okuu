#ifndef OPC_OPS_H
#define OPC_OPS_H

#define OP_COUNT 16

const char* op_str[OP_COUNT];
int (*op_func[OP_COUNT]) (unsigned int);

void run_address(unsigned int);

int opc_nop(unsigned int);
int opc_mov(unsigned int);
int opc_jmp(unsigned int);
int opc_jcd(unsigned int);
int opc_ari(unsigned int);
int opc_teq(unsigned int);
int opc_tgt(unsigned int);
int opc_tlt(unsigned int);
int opc_tru(unsigned int);
int opc_fls(unsigned int);
int opc_not(unsigned int);
int opc_end(unsigned int);


#endif
