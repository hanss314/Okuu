#ifndef OPC_REG_H
#define OPC_REG_H

#define REG_COUNT 16

const char* reg_str[REG_COUNT];
unsigned int (*reg_read[REG_COUNT]) ();
void (*reg_writ[REG_COUNT]) (unsigned int);

unsigned int read_nul();
unsigned int read_acc();
unsigned int read_rg0();
unsigned int read_rg1();
unsigned int read_rg2();
unsigned int read_rg3();
unsigned int read_dp0();
unsigned int read_dsk();
unsigned int read_dpt(); 
unsigned int read_mp0();
unsigned int read_mem();
unsigned int read_mpt();
unsigned int read_iop();
unsigned int read_nuk();

void writ_nul();
void writ_acc();
void writ_rg0();
void writ_rg1();
void writ_rg2();
void writ_rg3();
void writ_dp0();
void writ_dsk();
void writ_dpt(); 
void writ_mp0();
void writ_mem();
void writ_mpt();
void writ_iop();
void writ_nuk();


#endif
