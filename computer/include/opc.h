#include <stdio.h>
#include <stdbool.h>

#define RAM 65536 //64 KiB 16 bits
#define DISK 4194304 // 4MiB 22 bits

#define DISKFILE "computer/disk"

#ifndef OPC_H
#define OPC_H

FILE *disk;
unsigned int diskptr;
unsigned char ram[RAM];
bool flag;

#endif
