#include <stdbool.h>
#include <stdlib.h>
#include "ops.h"
#include "opc.h"
#include "reg.h"

const char* ops_str[OP_COUNT] = {
    "nop",
    "mov",
    "jmp",
    "jcd",
    "ari",
    "",
    "",
    "",
    "",
    "teq",
    "tgt",
    "tlt",
    "tru",
    "fls",
    "not",
    "end",
};

int (*op_func[OP_COUNT]) (unsigned int) = {
    &opc_nop,
    &opc_mov,
    &opc_jmp,
    &opc_jcd,
    &opc_ari,
    &opc_nop,
    &opc_nop,
    &opc_nop,
    &opc_nop,
    &opc_teq,
    &opc_tgt,
    &opc_tlt,
    &opc_tru,
    &opc_fls,
    &opc_not,
    &opc_end,
};

extern char* output;
extern char* input;
extern int olen;
extern int inpos;
extern int bufsize;

char read_disk_pos(unsigned int pos){
    unsigned char c;
    fseek(disk, pos/2, SEEK_SET);
    fread(&c, 1, 1, disk);
    
    if(pos%2==0){
        return c>>4;
    } else {
        return c%16;
    }
}

void run_address(unsigned int addr){
    addr *= 2;
    unsigned int head = addr;
    char op;
    unsigned int its=0;
    free(output); output = malloc(1); output[0] = '\0';
    olen = 0; bufsize = 1; inpos = 0;
    while(op != 15 && (its >> 16) < 1){
        op = read_disk_pos(addr); 
        int value = op_func[op](addr);
        if (value <= 0){
            addr = head-value;
        } else{
            addr = value;
        }
        its++;
    }

}

int opc_nop(unsigned int pos){
    return pos+1;
}

unsigned int read_port(unsigned int pos, unsigned int* nind){
    char src = read_disk_pos(pos);
    unsigned int val=0;
    if (src < 14){
        val = reg_read[src]();
        *nind = 1;
    } else {
        unsigned int chunks = 4*(src-13);
        for(unsigned int i=0; i<chunks; i++){
            val |= read_disk_pos(pos+i+1)<<(4*(chunks-i-1));
        }
        *nind = chunks+1;
    }
    return val;
}

int opc_mov(unsigned int pos){
    unsigned int nind=0;
    unsigned int val=read_port(pos+1, &nind);
    unsigned int dst = read_disk_pos(pos+nind+1);
    reg_writ[dst](val);
    return pos+nind+2;
}

int opc_jmp(unsigned int pos){
    return -read_acc();
}

int opc_jcd(unsigned int pos){
    if(flag) return opc_jmp(pos);
    return pos+1;
}

int opc_ari(unsigned int pos){
    char op = read_disk_pos(pos+1);
    unsigned int nind=0;
    unsigned int val=read_port(pos+2, &nind);
    switch(op){
        case  0: writ_acc(read_acc() + val); break;
        case  1: writ_acc(read_acc() - val); break;
        case  2: writ_acc(val - read_acc()); break;
        case  3: writ_acc(read_acc() * val); break;
        case  4: writ_acc(read_acc() / val); break;
        case  5: writ_acc(val / read_acc()); break;
        case  6: writ_acc(read_acc() % val); break;
        case  7: writ_acc(val % read_acc()); break;
        case  8: writ_acc(read_acc() | val); break;
        case  9: writ_acc(read_acc() & val); break;
        case 10: writ_acc(read_acc() ^ val); break;
        case 11: writ_acc(read_acc()); break;
        case 12: writ_acc(read_acc()); break;
        case 13: writ_acc(read_acc()); break;
        case 14: writ_acc(read_acc()); break;
        case 15: writ_acc(~read_acc()); break;
        default: break;
    }
    
    return pos+nind+2;
}


int opc_teq(unsigned int pos){
    unsigned int nind=0;
    unsigned int val=read_port(pos+1, &nind);
    flag = (val == read_acc());
    return pos+nind+1;
}

int opc_tlt(unsigned int pos){
    unsigned int nind=0;
    unsigned int val=read_port(pos+1, &nind);
    flag = (val < read_acc());
    return pos+nind+1;
}

int opc_tgt(unsigned int pos){
    unsigned int nind=0;
    unsigned int val=read_port(pos+1, &nind);
    flag = (val > read_acc());
    return pos+nind+1;
}
int opc_tru(unsigned int pos){
    flag = true;
    return pos+1;
}

int opc_fls(unsigned int pos){
    flag = false;
    return pos+1;
}

int opc_not(unsigned int pos){
    flag = !flag;
    return pos+1;
}

int opc_end(unsigned int pos){
    return pos+1;
}
