def quartopiece(arg):
    arg = arg.strip().lower()
    if len(arg) != 4:
        raise ValueError('piece must have a length of 4')

    piece = [None, None, None, None]
    for c in arg:
        if c not in QuartoGame.letters:
            raise ValueError(f'expected one of `cqslhfrb`, got `{c}`')
        ind = QuartoGame.letters.index(c)
        tf = bool(ind % 2)
        ind //= 2
        if piece[ind] is not None:
            raise ValueError(f'duplicate definition of {QuartoGame.attrs[ind]}')

        piece[ind] = tf

    return tuple(piece)



class QuartoGame:
    letters = 'cqslhfrb'
    attrs = ['shape', 'size', 'structure', 'color']
    def __init__(self, g, p):
        self.board = [[None]*4 for _ in range(4)]
        self.avail = {
            (*map(bool, (a,b,c,d)),)
            for a in range(2)
            for b in range(2)
            for c in range(2)
            for d in range(2)
        }
        self.g = g
        self.players = [p]
        self.turn = 0

    def __getitem__(self, item):
        return self.board[item[1]][item[0]]

    def __setitem__(self, key, value):
        self.board[key[1]][key[0]] = value

    def add_player(self, p):
        self.players.append(p)

    def make_move(self, x, y, piece):
        if piece not in self.avail: return -1, 'that piece has been used'
        if self[x,y] is not None: return -1, 'a piece is there'
        self[x,y] = piece
        self.avail.remove(piece)
        win = self.check_win(x, y)
        if win == -1:
            self.turn = (self.turn+1)%2
            return 0, 0
        else:
            return 1, win

    def check_win(self, x, y):
        curr = self[x,y]
        tests = [
            [self[x,dy] for dy in range(4)],
            [self[dx, y] for dx in range(4)],
            [self[i, i] for i in range(4)],
            [self[i, 3-i] for i in range(4)]
        ]

        for suite in tests:
            winnable = [True] * 4
            for test in suite:
                if test is None: break
                for i in range(4):
                    if test[i] != curr[i]: winnable[i] = False

                if not any(winnable): break
            else:
                return self.turn

        return -1


    def is_turn(self, p):
        if self.turn > len(self.players)-1: return False
        return p == self.players[self.turn]

    def __repr__(self):
        header = '  ' + '  '.join(f'{i}' for i in range(4))
        s = header + '\n'
        for n, row in enumerate(self.board):
            s += f'{n} '
            for p in row:
                if p is None: s += 'xx '
                else:
                    s += self.letters[0+p[0]] + self.letters[2+p[1]] + ' '

            s += '\n  '
            for p in row:
                if p is None: s += 'xx '
                else:
                    s += self.letters[4+p[2]] + self.letters[6+p[3]] + ' '

            s += '\n\n'

        return s
