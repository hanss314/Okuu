import hashlib
import math
import random
import os
import csv
import discord
import time
import asyncio

from .util.data import get_data, save_data
from computer import opc
from string import ascii_lowercase
from ruamel import yaml

from discord.ext import commands
Cog = commands.Cog

ascii_digit = '0123456789'
wrongs = [
    'Oof',
    'Nope',
    'Not quite',
    'Almost',
    'Not a chance',
    'Are you even trying?',
    'Way off',
    'So close',
]

def number(arg: str) -> complex:
    arg = arg.replace('i', 'j')
    return complex(arg)

def opcodes(arg: str):
    """Converts either bin or hex to bytes"""
    b = []
    if arg.count('0') + arg.count('1') == len(arg):
        arg += '0' * (8 - (len(arg)+1 % 8) + 1)
        for i in range(0, len(arg), 8):
            b.append(int(arg[i:i+8], base=2))

    else:
        arg = arg.lower()
        if len(arg) % 2: arg += '0'
        for i in range(0, len(arg), 2):
            b.append(int(arg[i:i + 2], base=16))

    return b



def factor(x):
    fs = set()
    for i in range(1, min(100000, int(math.ceil(math.sqrt(x))))):
        if x%i == 0:
            fs.add(x//i)
            fs.add(i)
    return fs

def p_factor(x):
    factors = set()
    i = 2
    while x > i:
        if x % i == 0:
            factors.add(i)
            x //= i
        else:
            i += 1
            
        if i > 100000:
            break

    return factors

class Fun(Cog):

    def __init__(self, bot):
        import os
        self.bot = bot
        self.wangernumb = False
        if os.path.exists('./bot_data/wangs.yml'):
            wangs = yaml.safe_load(open('./bot_data/wangs.yml'))
        else:
            wangs = {'lastwang': self.hash(1), 'leaderboard': {}}
            yaml.dump(wangs, open('./bot_data/wangs.yml', 'w'))

        if os.path.exists('./bot_data/ships.yml'):
            self.ships = yaml.safe_load(open('./bot_data/ships.yml'))
        else:
            self.ships = {}
            yaml.dump(self.ships, open('./bot_data/ships.yml', 'w'))

        with open('./config/fight_moves.csv', 'r') as fightcsv:
            reader = csv.reader(fightcsv)
            next(reader)
            self.fightmoves = list(reader)
            self.moveweights = list(map(lambda x: float(x[3]), self.fightmoves))


        self.fighthist = get_data('fighthist.yml')

        self.lastwang = wangs['lastwang']
        self.leaderboard = wangs['leaderboard']
        self.fightcooldown = {}

    @staticmethod
    def hash(object):
        return hashlib.sha512(bytes(repr(object), 'utf8')).hexdigest()

    @staticmethod
    def to_int(string: str) -> int:
        try: return int(string)
        except ValueError: return 1

    def write_wangfile(self):
        yaml.dump(
            {
                'lastwang': self.lastwang,
                'leaderboard': self.leaderboard,
                'typing': self.typing
            },
            open('./bot_data/wangs.yml', 'w')
        )

    def check_numberwang(self, num):
        r = num
        i = num.__dir__
        rstr = ''.join(c for c in self.hash(r) if c in ascii_lowercase)
        istr = self.to_int(''.join(c for c in self.hash(i) if c in ascii_digit))
        rstr = self.to_int(''.join(c for c in self.hash(rstr) if c in ascii_digit))
        lw = self.to_int(''.join(c for c in self.lastwang if c in ascii_digit))

        lw, istr = int(str(lw)[-int(str(istr)[0])//2-5:]), int(str(istr)[-int(str(lw)[0])//2-5:])
        lw = self.to_int(''.join(c for c in self.hash(factor(lw) ^ factor(istr)) if c in ascii_digit))
        lw, rstr = int(str(lw)[-int(str(rstr)[0]) // 2 - 5:]), int(str(rstr)[-int(str(lw)[0]) // 2 - 5:])
        final =  p_factor(lw) & p_factor(rstr)
        return len(final) > 0, len(final) > 1

    @commands.group(invoke_without_command=True)
    @commands.cooldown(1, 10, commands.BucketType.user)
    @commands.guild_only()
    async def numberwang(self, ctx, *, num):
        '''
        See if a number is numberwang.
        '''
        async with ctx.typing():
            is_numberwang, wangernumb = self.check_numberwang(num)


        if is_numberwang:
            self.lastwang = self.hash(self.lastwang + str(num))
            if not self.wangernumb:
                await ctx.send(f'{ctx.author.mention} That\'s numberwang!')
                self.lastwang = self.hash(str(num)+self.lastwang[0])
                try: self.leaderboard[ctx.guild.id][ctx.author.id] += 1
                except KeyError: self.leaderboard[ctx.guild.id][ctx.author.id] = 1
                if wangernumb:
                    self.wangernumb = True
                    await ctx.send('Let\'s rotate the board!')

            else:
                await ctx.send('That\'s WangerNumb!')
                self.wangernumb = False
                try: self.leaderboard[ctx.guild.id][ctx.author.id] += 2
                except KeyError: self.leaderboard[ctx.guild.id][ctx.author.id] = 2

            self.write_wangfile()

        else:
            if not self.wangernumb:
                await ctx.send(f'I\'m sorry {ctx.author.mention}, but that is not numberwang.')
                self.lastwang = self.hash(self.lastwang + str(num))
                self.write_wangfile()
            else:
                await ctx.send(random.choice(wrongs))

    @numberwang.command(name='leaderboard', aliases=['lead', 'lb'])
    @commands.cooldown(1, 15, commands.BucketType.channel)
    @commands.guild_only()
    async def numberwang_leaderboard(self, ctx):
        leaders = sorted([(score, uid) for uid, score in self.leaderboard[ctx.guild.id].items()], reverse=True)
        leaders = leaders[:10]
        d = '**__Leaderboard:__**\n'
        for n, k in enumerate(leaders):
            score, uid = k
            user = ctx.guild.get_member(uid)
            if user: line = f'**{user.name}**: '
            else: line = f'{uid}: '
            if ord(self.lastwang[-n-1]) % 2: line += 'Leading '
            else: line += 'Trailing '
            left = score*ord(self.lastwang[-n-2])//ord(self.lastwang[-n-3])
            right = ord(self.lastwang[-n-2])*ord(self.lastwang[-n-3])
            right %= (left*3//2) + 1
            if ord(self.lastwang[-n-2]) % 2: line += f'`{left} - {right}`'
            else: line += f'`{right} - {left}`'
            d += f'{line}\n'

        await ctx.send(d)

    @numberwang.before_invoke
    @numberwang_leaderboard.before_invoke
    async def check_guild_lb(self, ctx):
        if ctx.guild.id not in self.leaderboard:
            self.leaderboard[ctx.guild.id] = {}

    @commands.command()
    async def birb(self, ctx):
        """nuk brb"""
        await ctx.send('Tis a birb. A dangerous birb. An incredibly dangerous birb. Birb.')


    @commands.group(invoke_without_command=True)
    async def opc(self, ctx):
        """Okuu PC"""
        await ctx.send(
            "This is Okuu PC, a VM/emulator with only a basic "
            "interface for writing programs/data, "
            "running programs and some io.\n"
            f"Use `{ctx.prefix}help opc` for help."
        )

    @opc.command()
    async def write(self, ctx, location: int, code: opcodes):
        """Write a sequence of opcodes to a specified location on the disk
        Opcodes can be specified in either hex or binary.
        The end is padded with zeroes to the nearest byte.
        Data is stored big endian
        """
        await ctx.send('Writing')
        opc.set_diskptr(location)
        for c in code:
            try:
                opc.write_disk(c)
            except IndexError:
                return await ctx.send('The disk only has 4MiB of storage!')

        await ctx.send('Done!')

    @opc.command()
    async def read(self, ctx, location: int, size: int):
        """Returns <size> bytes in hex starting at <location>
        """
        opc.set_diskptr(location)
        s = ''
        for _ in range(size):
            try:
                s += f'{opc.read_disk():X}'
            except IndexError:
                return await ctx.send('The disk only has 4MiB of storage!')

        await ctx.send(s.lower())

    @opc.command()
    async def run(self, ctx, location: int, *, args=''):
        """Run code at a given location on the disk

        There is currently a hard limit of 2**16 instructions per run call.
        This number will be raised in the future as logistics get worked out.
        """
        opc.run_address(location, args)
        out = opc.get_output().decode('utf-8').strip()
        await ctx.send(out if out else "There was no output")

    @opc.command(aliases=['docs'])
    async def documentation(self, ctx, *path):
        """Get documentation on Okuu PC"""
        curr = 'computer/docs'
        for n, p in enumerate(path):
            if not os.path.isdir(curr):
                return await ctx.send(f'No entries in `{" ".join(path[:n])}`')

            lst = os.listdir(curr)
            avail = [e.split('.')[0] for e in lst]
            avail = [e for e in avail if e]
            if p == 'index' and n == len(path) - 1:
                msg = f'**Entries'
                if n > 0: msg += f' in `{" ".join(path[:n])}` '
                msg +='**\n'
                msg += '\n'.join(avail)
                return await ctx.send(msg)

            possible = [x for x in os.listdir(curr) if x in (p, p+'.md')]
            if not possible:
                curr_path = path[:n]
                msg = f'Entry `{p}` not found'
                if curr_path: msg += f"in `{' '.join(curr_path)}`"
                msg += '. Available entries: '
                msg += ', '.join(avail) + '.'
                return await ctx.send(msg)

            if len(possible) > 1 and n == len(path)-1:
                possible = [x for x in possible if '.md' in possible]
            elif len(possible) > 1:
                possible = [x for x in possible if '.md' not in possible]

            curr = os.path.join(curr, possible[0])

        if os.path.isdir(curr): curr = os.path.join(curr, '.md')

        with open(curr, 'r') as docs:
            documentation = self.format_docs(docs.read())
            await ctx.send(documentation.format(pfx=ctx.prefix))


    @staticmethod
    def format_docs(text):
        """Formats some docs"""
        return text.replace(' \\\n', ' ').replace('\\\n', ' ')

    @commands.group(invoke_without_command=True)
    @commands.guild_only()
    async def ship(self, ctx):
        """Ship two random people!"""
        ships = self.ships.get(ctx.guild.id, [])
        if not ships: await ctx.send("No one has opted in to being shipped!")
        aid = random.choice(ships)
        bid = random.choice(ships)
        a = self.bot.get_user(aid)
        b = self.bot.get_user(bid)
        message = f"I ship {a.name} x {b.name}!"
        if aid == bid: message += ' Selfcest!'
        return await ctx.send(message)


    @ship.command(name='in')
    @commands.guild_only()
    async def ship_in(self, ctx):
        """Give consent to being shipped"""
        if ctx.guild.id not in self.ships:
            self.ships[ctx.guild.id] = []

        guild = self.ships[ctx.guild.id]
        if ctx.author.id in guild: return await ctx.send("You've already given consent!")
        guild.append(ctx.author.id)
        yaml.dump(self.ships, open('./bot_data/ships.yml', 'w'))
        return await ctx.send("Consent noted.")


    @ship.command(name='out')
    @commands.guild_only()
    async def ship_out(self, ctx):
        """Revoke consent to being shipped"""
        if ctx.guild.id not in self.ships:
            self.ships[ctx.guild.id] = []

        guild = self.ships[ctx.guild.id]
        if ctx.author.id not in guild: return await ctx.send("You haven't given consent!")
        guild.remove(ctx.author.id)
        yaml.dump(self.ships, open('./bot_data/ships.yml', 'w'))
        return await ctx.send("Consent revoked.")

    @ship.command(name='me')
    @commands.guild_only()
    async def ship_me(self, ctx):
        """Ship yourself with someone"""
        ships = self.ships.get(ctx.guild.id, [])
        if not ships: await ctx.send("No one has opted in to being shipped!")
        aid = random.choice(ships)
        a = self.bot.get_user(aid)
        message = f"I ship {ctx.author.name} x {a.name}!"
        if ctx.author.id == aid: message += ' Selfcest!'
        return await ctx.send(message)

    @commands.group(invoke_without_command=True)
    @commands.guild_only()
    async def fight(self, ctx, *, user: discord.Member):
        """Fight someone!"""
        lastinvocation = self.fightcooldown.get(ctx.author.id, 0)
        distance = time.time() - lastinvocation
        if distance < 60:
            return await ctx.send(f'Overheating! Try again in {60-distance} seconds.')

        if ctx.author.id == user.id: return await ctx.send('You can\'t fight yourself!')
        self.fightcooldown[ctx.author.id] = time.time()
        fighters = [[ctx.author, 20], [user, 20]]
        turn = 0
        events = f'**{fighters[0][0].name}** *vs* **{fighters[1][0].name}**\n\n'
        while fighters[0][1] > 0 and fighters[1][1] > 0:
            move = random.choices(self.fightmoves, weights=self.moveweights)[0]
            if isinstance(move[1], str):
                for i in (1, 2):
                    try: move[i] = int(move[i])
                    except ValueError: move[i] = float(move[i])
            
            if fighters[1-turn][0].id == self.bot.user.id:
                while move[1] > 20:
                    move = random.choices(self.fightmoves, weights=self.moveweights)[0]

            if fighters[turn][0].id == self.bot.user.id:
                move = ['Bullshit Flare', 100000000, 0]
        

            damagetext = '... nothing happened'
            if move[1] != 0:
                damagetext = f'and dealt {move[1]} damage'
                if move[2] != 0:
                    damagetext += f' but hurt themself, dealing {move[2]} damage.'
                else:
                    damagetext += '!'
                    
            elif move[2] != 0:
                damagetext = f'and dealt {move[2]} damage to themself!'

            health = fighters[turn][1]
            if isinstance(health, float):
                health = round(health, 2)
            fighters[turn][1] -= move[2]
            fighters[1-turn][1] -= move[1]

            events += f'`{fighters[turn][0].name}[{health}]` used **{move[0]}** {damagetext}\n'
            turn = 1-turn
        
        nondead = [fighter for fighter, health in fighters if health > 0]
        if len(nondead) != 1:
            events += 'Simultaneous knockout! It\'s a tie!'
        else:
            winner = nondead[0]
            events += f'{winner.name} wins!'
            if ctx.guild.id not in self.fighthist:
                self.fighthist[ctx.guild.id] = {}
            if winner.id not in self.fighthist[ctx.guild.id]:
                self.fighthist[ctx.guild.id][winner.id] = 0
            self.fighthist[ctx.guild.id][winner.id] += 1

        await ctx.trigger_typing()
        await asyncio.sleep(2)
        await ctx.send(events)

    @fight.command(name='stats')
    @commands.guild_only()
    async def fightstats(self, ctx, page: int=1):
        """See fight win history"""
        fighters = self.fighthist[ctx.guild.id]
        fighters = sorted(fighters.items(), key=lambda x: x[1], reverse=True)
        scoreboard = ''
        ranked = list(map(lambda x: x[0], fighters))
        try:
            userrank = ranked.index(ctx.author.id)
        except ValueError: pass
        else:
            scoreboard = f'Your rank: **{1+userrank}/{len(fighters)}**\n\n'

        if page < 1 or page > len(fighters)//10+1:
            await ctx.send

        fighters = list(map(lambda l: (ctx.guild.get_member(l[0]), l[1]), fighters))
        fighters = list(enumerate(fighters))[10*(page-1):10*page]
        scoreboard += '\n'.join(f'{f[0]+1}. **{f[1][0].name}** - {f[1][1]} wins' for f in fighters)
        await ctx.send(scoreboard)

    @fight.command(name='request')
    async def fightrequest(self, ctx, *, request):
        """Request a fight move"""
        await self.bot.get_user(240995021208289280).send(f'**{ctx.author}** requested `{request}`')
        await ctx.send("Request sent!")

    @fight.after_invoke
    async def save_fights(self, _):
        save_data('fighthist.yml', self.fighthist)

def setup(bot):
    bot.add_cog(Fun(bot))
