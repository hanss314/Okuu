from pyparsing import Literal, Group, ZeroOrMore, Forward, Regex,\
    CaselessKeyword, Suppress, dblQuotedString, ParseException, restOfLine
from functools import reduce
from .rpn import symbol_regex

ParseException = ParseException

consts = {
    "pi",
    "e"
}
opn = [
    ("+", 2),
    ("-", 2),
    ("*", 1),
    ("/", 1),
    ("//", 1),
    ("%", 1),
    ("^", 0),
    ("**", 0),
    ('&', 4),
    ('^|', 5),
    ('|', 6),
    ('<<', 3),
    ('>>', 3),
    ('==', 7),
    ('!=', 7),
    ('<=', 7),
    ('<', 8),
    ('>', 8),
    ('>=', 8),
    (':=', 10)
]
fn  = {
    "sqrt": 1,

    'sin': 1,
    'cos': 1,
    'tan': 1,
    'asin': 1,
    'acos': 1,
    'atan': 1,

    'sinh': 1,
    'cosh': 1,
    'tanh': 1,
    'asinh': 1,
    'acosh': 1,
    'atanh': 1,

    'ceil': 1,
    'flr': 1,
    'abs': 1,

    'ln': 1,
    'log': 1,
    'logb': 2,

    'eval': 1,
    'evals': 1,
    'veval': 1,
    'vevals': 1,
    'cond': 3,
    'del': 1,
}


fnumber = Regex(r"-?\d+(?:\.\d*)?(?:[eE][+-]?\d+)?[ij]?")
name_symbol = Regex(symbol_regex)
lpar, rpar = map(Suppress, "()")
constclass = reduce(lambda x,y:x^y, map(CaselessKeyword, consts))

def get_reps(ex, count):
    if count == 0: return ''
    val = ex
    for i in range(count-1):
        val = val + ',' + ex

    return val

def get_idents(ex):
    idents = None
    for name, args in fn.items():
        if idents is None:
            idents = name + lpar + get_reps(ex, args) + rpar
        else:
            idents = idents ^ (name + lpar + get_reps(ex, args) + rpar)

    return idents



ops = {}
for op in opn:
    if op[1] not in ops:
        ops[op[1]] = Literal(op[0])
    else:
        ops[op[1]] ^= Literal(op[0])

ops = list(ops.items())
ops.sort(key=lambda x:x[0])

class BFN():
    def __init__(self):
        self.exprStack = []
        expr = Forward()
        comp = (0, None) * Literal('~')
        atom = constclass | fnumber | get_idents(expr) | \
               dblQuotedString | name_symbol

        parend = Group(lpar + expr + rpar)
        atom = atom.setParseAction(self._push_first) | parend
        term = Forward()
        term << atom + ZeroOrMore((ops[0][1] + term).setParseAction(self._push_first))
        term = (comp + term).setParseAction(self._push_comp)
        for n, op in ops[1:]:
            term = term + ZeroOrMore((op + term).setParseAction(self._push_first))
            if n == 2:
                neg = (0,None) * Literal("-")
                term = (neg + term).setParseAction(self._push_uminus)

        expr << term
        self.expr = expr + (0, None) * (';' + expr)

    def _push_first(self, _, __, toks):
        self.exprStack.append(toks[0])

    def _push_comp(self, _, __, toks):
        for t in toks:
            if t == '~': self.exprStack.append('~')
            else: break

    def _push_uminus(self, _, __, toks):
        for t in toks:
            if t == '-':
                self.exprStack.append( '-1' )
                self.exprStack.append( '*' )
            else:
                break

    def parse(self, expr):
        results = list(self.expr.scanString(expr))
        if not results: raise ParseException("Nothing matched")
        toks, start, end = results[0]
        return self.exprStack, end - start
