import json
import os

from ruamel import yaml

BASE_FOLDER = 'bot_data/{}'

registry = {}


def listdump():
    def dumpf(data, f):
        f.write('\n'.join(map(str, data)))
    return dumpf


def listload(converter):
    def loadf(f):
        return [converter(l.rstrip('\n')) for l in f.readlines() if l]
    return loadf


def load(filename, loader=None):
    if loader is not None:
        loader = loader
    elif filename.endswith('.json'):
        loader = json.load
    elif filename.endswith('.yml') or filename.endswith('.yaml'):
        loader = yaml.load
    else:
        raise ValueError('File must have a json or yaml extension')

    with open(filename, 'r') as data_file:
        data = loader(data_file)

    return data


def dump(data, filename, dumper=None):
    if dumper is not None:
        dumper = dumper
    elif filename.endswith('.json'):
        dumper = json.dump
    elif filename.endswith('.yml') or filename.endswith('.yaml'):
        dumper = yaml.dump
    else:
        raise("Please specify a dumper for non json or yaml files")

    with open(filename, 'w') as data_file:
        dumper(data, data_file)


def get_data(filename: str, base=dict, dumper=None, loader=None):
    filename = BASE_FOLDER.format(filename)
    if os.path.isfile(filename):
        data = load(filename, loader=loader)
    else:
        data = base()
        dump(data, filename, dumper=dumper)

    registry[filename] = data
    return data


def save_data(filename: str, data=None, dumper=None):
    filename = BASE_FOLDER.format(filename)
    if filename not in registry and data is None:
        raise ValueError('File has not been loaded')

    dump(registry[filename] if data is None else data, filename, dumper=dumper)


