class Symbol:
    def __init__(self, s): self.s = s
    def __eq__(self, other):
        if isinstance(other, Symbol): return self.s == other.s
        return False
    def __str__(self): return self.s
    def __repr__(self): return self.s

class ProgramState:
    def __init__(self, builtins, syntax, args):
        self.out = ''
        self.rec = 0
        self.vars = {k: [v] for k, v in builtins.items()}
        self.syn = dict(syntax)
        self.ctx, self.args, self.saves = args

    def new_scope(self):
        for v in self.vars.values():
            v.append(v[-1])
        self.rec += 1

    def pop_scope(self):
        for v in self.vars.values():
            v.pop()
        for k in list(self.vars):
            if not self.vars[k]: del self.vars[k]
        self.rec -= 1

class Function:
    def __init__(self, args, body):
        self.args = list(map(lambda x: x.s, args))
        self.body = body

    def __call__(self, state, args):
        from .eval import evaluate
        if len(args) != len(self.args):
            raise TypeError(f'Expected {len(self.args)} args got {len(args)}')
        for k, v in zip(self.args, args):
            if k in state.vars: state.vars[k][-1] = v
            else: state.vars[k] = [v]
        ret = None
        for statement in self.body:
            ret = evaluate(state, statement)
        return ret

    def __str__(self):
        return '[function object]'
    def __repr__(self):
        return '[function object]'

class ParseError(SyntaxError): pass