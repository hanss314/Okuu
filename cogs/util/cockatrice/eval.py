try:
    from .parser import parse_cock
    from .data import *
    from .funcs import builtins, syntaxes, flatten
except ImportError:
    from parser import parse_cock
    from data import *
    from funcs import builtins, syntaxes, flatten


def untree(t):
    items = []
    while isinstance(t, tuple):
        items.append(stringify(t[0]))
        t = t[1]
    if t is not None:
        items.append(f'. {repr(t)}')
    return '('+' '.join(items)+')'


def stringify(t):
    if t is None: return '()'
    elif isinstance(t, tuple): return untree(t)
    return repr(t)


def top_evaluate(e, args):
    output = ''
    state = ProgramState(builtins, syntaxes, args)
    for exp in e:
        if isinstance(exp, str): output += exp
        else:
            ret = evaluate(state, exp)
            output += state.out
            state.out = ''
            if ret is None: continue
            if isinstance(ret, tuple): output += stringify(ret)
            else: output += str(ret)
    return output


def run_func(state, name, args):
    syntax = False
    if isinstance(name, tuple): f = evaluate(state, name)
    elif isinstance(name, Symbol):
        if name.s in state.vars: f = state.vars[name.s][-1]
        elif name.s in state.syn:
            f = state.syn[name.s]
            syntax = True
        else:
            raise NameError(f'{name} is not defined')
    else:
        raise TypeError(f'Object is not callable')
    if not callable(f):
        print(f)
        raise TypeError(f'Object is not callable')
    args = flatten(args)
    if syntax: return f(state, *args)
    else:
        args = list(map(lambda x: evaluate(state, x), args))
        if isinstance(f, Function): return f(state, args)
        else: return f(*args)

def evaluate(state, e):
    if isinstance(e, Symbol):
        if e.s not in state.vars:
            if e.s in state.syn:
                raise NameError(f'Syntax keyword {e} is not an expression')
            else:
                raise NameError(f'Variable {e} is not defined')
        return state.vars[e.s][-1]
    elif isinstance(e, tuple):
        state.new_scope()
        val = run_func(state, e[0], e[1])
        state.pop_scope()
        return val

    return e

