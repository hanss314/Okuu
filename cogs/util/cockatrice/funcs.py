try:
    from .data import *
except ImportError:
    from data import *

import math
import random
import statistics as stats
import scipy.stats as sci
from numpy import random as nrand

def flatten(s):
    l = []
    while s is not None:
        l.append(s[0])
        s = s[1]
    return l

builtins = {'pi': math.pi, 'e': math.e}
def builtin(name=None):
    def wrapper(f):
        builtins[name or f.__name__] = f
        return f
    return wrapper

@builtin()
def car(p): return p[0]
@builtin()
def cdr(p): return p[1]
@builtin()
def cons(x, y): return x, y
@builtin(name='list')
def make_list(*args): return makeTree([list(args)])
@builtin(name='+')
def add(*args):return sum(args)
@builtin(name='-')
def sub(*args):
    if len(args) == 0: raise TypeError('Expected at least 1 argument, got 0')
    elif len(args) == 1: return -args[0]
    else: return args[0] - sum(args[1:])
@builtin(name='*')
def mul(*args):
    total = 1
    for x in args:
        if abs(x) >= 2**64: raise ValueError('Number is too large')
        total *= x
    return total
@builtin(name='/')
def div(*args):
    if len(args) == 0: raise TypeError('Expected at least 1 argument, got 0')
    elif len(args) == 1: return 1/args[0]
    return args[0] / mul(*args[1:])

@builtin(name='>')
def gt(*args):
    for i in range(len(args)-1):
        if args[i] <= args[i+1]: return False
    return True
@builtin(name='<')
def lt(*args):
    for i in range(len(args)-1):
        if args[i] >= args[i+1]: return False
    return True
@builtin(name='>=')
def ge(*args):
    for i in range(len(args)-1):
        if args[i] < args[i+1]: return False
    return True
@builtin(name='<')
def le(*args):
    for i in range(len(args)-1):
        if args[i] > args[i+1]: return False
    return True

@builtin(name='=')
def le(*args):
    for i in range(len(args)-1):
        if args[i] != args[i+1]: return False
    return True

@builtin()
def join(s, l):
    from .eval import stringify
    if not isinstance(s, str): raise TypeError('Expected string')
    l = flatten(l)
    return s.join(map(stringify, l))

@builtin(name='^')
def exponent(b, n): return math.pow(b, n)
@builtin()
def avg(*args):
    if isinstance(args[0], tuple): args = flatten(args[0])
    return stats.mean(args)
builtins['mean'] = avg
@builtin(name='stdev')
def std(*args):
    if isinstance(args[0], tuple): args = flatten(args[0])
    return stats.stdev(args)
@builtin()
def stdevp(*args):
    if isinstance(args[0], tuple): args = flatten(args[0])
    return stats.pstdev(args)
@builtin(name='skew')
def skw(*args):
    if isinstance(args[0], tuple): args = flatten(args[0])
    return sci.skew(args)
@builtin()
def kurt(*args):
    if isinstance(args[0], tuple): args = flatten(args[0])
    return sci.kurtosis(args)

@builtin()
def sqrt(x): return math.sqrt(x)
@builtin()
def log(*args):
    if len(args) == 1: return math.log(args[0])
    elif len(args) == 2: return math.log(args[0], args[1])
    else: raise TypeError(f'Expected 1 or 2 args, got {len(args)}')

@builtin()
def choice(*args): return random.choice(args)
@builtin()
def randint(a, b): return random.randint(a, b)
@builtin()
def pareto(x): return nrand.pareto(x)
@builtin()
def normal(x, s): return nrand.normal(x, s)
@builtin()
def uniform(l, h): return nrand.uniform(l, h)
@builtin(name='null?')
def isnull(x): return x is None
@builtin()
def sin(x): return math.sin(x)
@builtin()
def cos(x): return math.cos(x)
@builtin()
def tan(x): return math.tan(x)
@builtin()
def asin(x): return math.asin(x)
@builtin()
def acos(x): return math.acos(x)
@builtin()
def atan(x): return math.atan(x)
@builtin()
def sec(x): return 1/math.cos(x)
@builtin()
def csc(x): return 1/math.sin(x)
@builtin()
def cot(x): return 1/math.tan(x)
@builtin()
def asec(x): return math.acos(1/x)
@builtin()
def acsc(x): return math.asin(1/x)
@builtin()
def acot(x): return math.atan(1/x)
@builtin(name='int')
def toint(x):
    try: return int(x)
    except ValueError: return None

@builtin(name='float')
def tofloat(x):
    try: return float(x)
    except ValueError: return None

@builtin(name='str')
def tostr(x):
    try: return str(x)
    except ValueError: return None

syntaxes = {}
def syntax(name=None):
    def wrapper(f):
        syntaxes[name or f.__name__] = f
        return f
    return wrapper

@syntax(name='if')
def if_statement(state, cond, true, *false):
    from .eval import evaluate
    if evaluate(state, cond):
        return evaluate(state, true)
    ret = None
    for statement in false:
        ret = evaluate(state, statement)
    return ret

@syntax()
def out(state, *args):
    from .eval import evaluate, stringify
    for arg in args:
        x = evaluate(state, arg)
        if isinstance(x, tuple) or x is None: state.out += stringify(x)
        else: state.out += str(x)

@syntax()
def define(state, name, *value):
    from .eval import evaluate
    if isinstance(name, Symbol):
        if len(value) != 1: raise SyntaxError('Malformed define syntax')
        value = evaluate(state, value[0])
    elif isinstance(name, tuple):
        if len(value) == 0: raise SyntaxError('Malformed define syntax')
        name = flatten(name)
        for arg in name:
            if not isinstance(arg, Symbol):
                raise SyntaxError('Maformed define syntax')

        name, args = name[0], name[1:]
        value = Function(args, list(value))
    else:
        raise SyntaxError('Maformed define syntax')
    name = name.s
    if name in state.vars: state.vars[name][-2] = value
    else: state.vars[name] = [value, value]

@syntax()
def suppress(state, *instrs):
    from .eval import evaluate
    for e in instrs: evaluate(state, e)

@syntax()
def quote(state, x): return x
@syntax()
def quasiquote(state, x):
    from .eval import evaluate
    if isinstance(x, tuple):
        if Symbol('unquote') == x[0]: return evaluate(state, x[1][0])
        return quasiquote(state, x[0]), quasiquote(state, x[1])
    return x

@syntax()
def eval(state, *args):
    from .eval import evaluate
    r = None
    for p in args:
        r = evaluate(state, evaluate(state, p))
    return r

@syntax()
def apply(state, *args):
    from .parser import makeTree
    from .eval import evaluate
    f = args[0]
    args = list(map(lambda x: evaluate(state, x), args[1:]))
    params = args[:-1] + flatten(args[-1])
    params = makeTree((params,))
    return evaluate(state, (f, params))

@syntax(name='arg')
def getarg(state, name):
    from .parser import makeTree
    ctx = state.ctx
    argv = state.args
    S = Symbol
    if isinstance(name, int):
        try: return argv[name]
        except IndexError: return None
    if name == S('all'): return makeTree((argv,))
    if name == S('argc'): return len(argv)
    if name == S('uid'): return ctx.author.id
    if name == S('uname'): return ctx.author.name
    if name == S('cid'): return ctx.channel.id
    if name == S('cname'): return ctx.channel.name
    if name == S('gid'): return ctx.guild.id
    if name == S('gname'): return ctx.guild.name
    raise NameError('Argument not found')

@syntax()
def read(state, name):
    return state.saves.get(name.s, None)

@syntax()
def write(state, name, val):
    from .eval import evaluate
    if not isinstance(name, Symbol): raise TypeError('Expected Symbol')
    val = evaluate(state, val)
    if isinstance(val, int) or isinstance(val, float) or \
        isinstance(val, str) or val is None:
        state.saves[name.s] = val
    elif isinstance(val, tuple): state.saves[name.s] = flatten(val)
    else: raise TypeError('Can only write lists, numbers and strings')
    return val

@syntax(name='map')
def cockmap(state, f, xs):
    from .eval import evaluate
    from .parser import makeTree
    xs = flatten(evaluate(state, xs))
    xs = list(map(lambda x: evaluate(state, (f, (x, None))), xs))
    return makeTree((xs,))

@syntax(name='filter')
def cockfilter(state, f, xs):
    from .eval import evaluate
    from .parser import makeTree
    xs = flatten(evaluate(state, xs))
    xs = list(filter(lambda x: evaluate(state, (f, (x, None))), xs))
    return makeTree((xs,))

