from pyparsing import Literal, Group, ZeroOrMore, Forward, Regex, White,\
    OneOrMore, Suppress, dblQuotedString, ParseResults, removeQuotes, Combine

try: from .data import *
except ImportError: from data import *


def makeTree(t):
    root = None
    for x in reversed(t[0]):
        if isinstance(x, ParseResults) and len(x) == 0: x = None
        root = x, root
    return root

def makeQuote(t):
    quotes = {
        "'": 'quote',
        "`": 'quasiquote',
        ",": 'unquote'
    }
    return Symbol(quotes[t[0]]), (t[1], None)


quote = Regex('[`\',]')
LPAR = Suppress('(')
RPAR = Suppress(')')
symbol = Regex(r"[^() \t\n'`,][^() \t\n]*").setParseAction(lambda t: Symbol(t[0]))
integer = Regex(r"-?[0-9]+").setParseAction(lambda t: int(t[0]))
floating = Regex(r"-?[0-9]+\.[0-9]+").setParseAction(lambda t: float(t[0]))
string = dblQuotedString.setParseAction(removeQuotes)
atom = floating | integer | string | symbol
atom = (quote + atom).setParseAction(makeQuote) | atom

sexp = Forward()
sexpList = Group(LPAR + sexp[...] + RPAR).setParseAction(makeTree)
qsexpList = (quote + sexpList).setParseAction(makeQuote)
sexpList = qsexpList | sexpList
sexp <<= atom | sexpList
ELPAR = Literal('\\(')
ERPAR = Literal('\\)')
sentence = OneOrMore(ELPAR ^ ERPAR ^ Regex('[^()]'))
sentence = Combine(sentence)
program = ZeroOrMore(sexpList | White() | sentence)

def parse_cock(prog):
    prog = prog.strip()
    x = list(program.scanString(prog))
    p, s, e = x[0]
    if s != 0: raise ParseError(f'Parse failure at position {s}')
    elif e != len(prog): raise ParseError(f'Parse failure at position {e}')
    return p


if __name__ == '__main__':
    test = """(define (fibs n)
            (define (iter-fibs n a b) (if (= n 0) ()
                (cons a (iter-fibs (- n 1) b (+ a b)))))
            (iter-fibs n 0 1))
        (define (lp) (out "("))
        (define (rp) (out ")"))
        Fibonacci numbers: (lp)(join " " (fibs 20)) (+ 21 34)(rp)"""
    test  = "'(hi) hi"
    test = test.strip()
    x = list(program.scanString(test))
    print(x)
