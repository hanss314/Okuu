import asyncio

from .parser import parse_cock as parse
from .eval import top_evaluate as evaluate
from .data import *

async def run_program(prog, args=None):
    if args is None: args = []
    prog = prog.strip()
    try: prog = parse(prog)
    except ParseError as e:
        return e.msg
    loop = asyncio.get_event_loop()
    executor = loop.run_in_executor(None, evaluate, prog, args)
    try:
        result = await asyncio.wait_for(executor, timeout=2)
    except asyncio.TimeoutError:
        return 'Evaluation timed out'
    except Exception as e:
        # raise e
        return str(e)
    else:
        return result

