VOWELS = 'aioe'
NONENDS = 'ptkcmsywh'

SYMBOLS = 'ᐁᐃᐅᐊᐯᐱᐳᐸᑌᑎᑐᑕᑫᑭᑯᑲᒉᒋᒍᒐᒣᒥᒧᒪᓀᓂᓄᓇᓭᓯᓱᓴᔦᔨᔪᔭ'
VOWELS = 'eioa'
CONSAS = 'ptkcmnsy'

MAP = {v: s for v, s in zip(VOWELS, SYMBOLS)}
MAP.update({'the': 'ᖧ', 'thi': 'ᖨ', 'tho': 'ᖪ', 'tha': 'ᖬ'})

for i in range(len(CONSAS)):
    for j in range(len(VOWELS)):
        MAP[CONSAS[i]+VOWELS[j]] = SYMBOLS[(i+1)*4 + j]

for v, s in zip(VOWELS, SYMBOLS):
    MAP['w'+v] = s+'·'

MAP.update({k+k[-1]: v+'\u0307' for k, v in MAP.items()})
CONSAS += 'whlr'
ENDSYMS = 'ᑊᐟᐠᐨᒼᐣᐢᐝᐤᐦᓫᕑ'
MAP.update({v: s for v, s in zip(CONSAS, ENDSYMS)})
MAP.update({'hk': 'ᕽ', 'th': 'ᙾ'})
MAP.update({' ': ' '})

def parse_cas(s):
    syllables = get_syllables(s)
    return ''.join(map(MAP.__getitem__, syllables))

def get_syllables(s):
    sylls = []
    for w in s.split(' '):
        while w:
            syll, w = get_single_syllable(w)
            sylls.append(syll)

        sylls.append(' ')

    return sylls[:-1]

def get_vowel(s):
    syll = ''
    consumed = 0
    if not s: return '', s
    if s[0] in VOWELS and len(s) > 1 and s[1] == s[0]:
        syll += s[:2]
        consumed += 2
    elif s[0] in VOWELS:
        syll += s[0]
        consumed += 1
    else:
        return '', s

    if len(s) > consumed and s[consumed] in "'-": consumed += 1

    return syll, s[consumed:]

def get_single_syllable(s):
    syll = ''
    if len(s) < 2: return s, ''
    if s.startswith('hk'):
        return 'hk', s[2:]
    elif s.startswith('th'):
        syll, s = 'th', s[2:]
    elif s[0] in 'lrh':
        return s[0], s[1:]
    elif s[0] not in VOWELS:
        syll, s = s[0], s[1:]

    vowels, s = get_vowel(s)
    return syll+vowels, s

if __name__ == '__main__':
    print(parse_cas('pe\'e'))
