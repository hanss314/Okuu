import aiohttp
import re
from urllib.parse import quote as uriquote

XKCD_SEARCH = re.compile(r""" href="https://xkcd\.com/([0-9]{1,5})/" """)

GPARAMS = {'safe': 'on', 'lr': 'lang_en', 'hl': 'en'}
GHEADERS = {'User-Agent': 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) Gecko/20100101 Firefox/53.0'}


async def get_google_xkcd(query):
    query = 'xkcd' + query
    url = f'https://www.google.com/search?q={uriquote(query)}'

    matches = []
    async with aiohttp.ClientSession() as session:
        async with session.get(url, params=GPARAMS, headers=GHEADERS) as resp:
            if resp.status != 200:
                raise RuntimeError('Google has failed to respond.')

            text = await resp.text()
            matches = XKCD_SEARCH.findall(text)

    print(matches)
    return matches