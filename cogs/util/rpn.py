import math
import statistics
import re

symbol_regex = r"[a-zA-Z_][a-zA-Z0-9_]*"

help_messages = {
    'strings': 'Use double quotes `"` to delimit strings. '
               'Use a backslash to have double quotes in your string. '
               'Use "\\n" for a newline character.'
}

def delval(v, name):
    if not issym(name):
        raise ValueError("Cannot delete non symbol")

    del v[sym[1]]

def pop(l, v, *args):
    q = l.pop(*args)
    if issym(q):
        if q[1] in v: return v[q[1]]
        raise KeyError(f"Name {q[1]} is not defined")
    
    return q

def limexp(e, b):
    if abs(e) > 2048:
        raise MemoryError("Exponent is too great")
    elif abs(b) > 100000000000:
        raise MemoryError("Base is too great")

    return b**e

def setvar(val, name, heap):
    if not issym(name):
        raise ValueError("Attempted to assign to non symbol")

    heap[name[1]] = val

def issym(name):
    return isinstance(name, tuple) and len(name) == 2 and name[0] == 'sym'

def makefun(body, args):
    args = args.split(' ')
    for arg in args:
        try:
            std_symbol(arg)
        except ValueError:
            raise ValueError("Arguments must be symbols")

    return 'fn', (tuple(args), split_args(body))

def safe_mul(x, y):
    if abs(x) > 2**64 or abs(y) > 2**64: raise ValueError('Numbers are too large to multiply')
    return x*y

rpncalc = {
    # constants
    'pi': ('Pushes the mathematical constant pi', lambda l, v: l.append(math.pi)),
    'e': ('Pushes euler\'s constant', lambda l, v: l.append(math.e)),

    # arithmetic
    '+': ('Sum of two numbers', lambda l, v: l.append(pop(l,v,-2) + pop(l,v))),
    '-': ('Difference of two numbers', lambda l, v: l.append(pop(l,v,-2) - pop(l,v))),
    '*': ('Multiply two numbers', lambda l, v: l.append(safe_mul(pop(l,v,-2), pop(l,v)))),
    '/': ('Divide two numbers', lambda l, v: l.append(pop(l,v,-2) / pop(l,v))),
    '//': ('Division, rounded down', lambda l, v: l.append(pop(l,v,-2) // pop(l,v))),
    '^': ('Exponentiation with math.pow', lambda l, v: l.append(math.pow(pop(l,v,-2), pop(l,v)))),
    '**': ('Exponentiation with **, maximum exponent is 2048',
           lambda l, v: l.append(limexp(pop(l,v), pop(l,v)))),
    '%': ('Modulo second on stack by first', lambda l, v: l.append(pop(l,v,-2) % pop(l,v))),
    'sqrt': ('Pops one number, pushes its square root', lambda l, v: l.append(pop(l,v) ** 0.5)),

    # trig
    'sin': ('Sine operator', lambda l, v: l.append(math.sin(pop(l,v)))),
    'cos': ('Cosine operator', lambda l, v: l.append(math.cos(pop(l,v)))),
    'tan': ('Tangent operator', lambda l, v: l.append(math.tan(pop(l,v)))),
    'asin': ('Inverse sine', lambda l, v: l.append(math.asin(pop(l,v)))),
    'acos': ('Inverse cosine', lambda l, v: l.append(math.acos(pop(l,v)))),
    'atan': ('Inverse tangent', lambda l, v: l.append(math.atan(pop(l,v)))),

    # hyperbolic
    'sinh': ('Hyperbolic sine', lambda l, v: l.append(math.sinh(pop(l,v)))),
    'cosh': ('Hyperbolic cosine', lambda l, v: l.append(math.cosh(pop(l,v)))),
    'tanh': ('Hyperbolic tangent', lambda l, v: l.append(math.tanh(pop(l,v)))),
    'asinh': ('Inverse hyperbolic sine', lambda l, v: l.append(math.asinh(pop(l,v)))),
    'acosh': ('Inverse hyperbolic cosine', lambda l, v: l.append(math.acosh(pop(l,v)))),
    'atanh': ('Inverse hyperbolic tangent', lambda l, v: l.append(math.atanh(pop(l,v)))),

    # logs
    'ln': ('Natural logarithm', lambda l, v: l.append(math.log(pop(l,v)))),
    'log': ('Log in base 10', lambda l, v: l.append(math.log10(pop(l,v)))),
    'logb': (
        'Pops two numbers, takes logarithm of first in base of second',
        lambda l, v: l.append(math.log(pop(l,v), pop(l,v)))
    ),

    # bitwise operators
    '&': ('Bitwise AND', lambda l, v: l.append(pop(l,v) & pop(l,v))),
    '|': ('Bitwise OR', lambda l, v: l.append(pop(l,v) | pop(l,v))),
    '^|': ('Bitwise XOR', lambda l, v: l.append(pop(l,v) ^ pop(l,v))),
    '!': ('Bitwise complement', lambda l, v: l.append(~pop(l,v))),
    '~': ('Bitwise complement', lambda l, v: l.append(~pop(l,v))),
    '<<': ('Pop two numbers, bitshift second left by first', lambda l, v: l.append(pop(l,v,-2) << pop(l,v))),
    '>>': ('Pop two numbers, bitshift second right by first', lambda l, v: l.append(pop(l,v,-2) >> pop(l,v))),

    # logical operators
    '==': ('Pushes `1` if top two equal, else `0`', lambda l, v: l.append(int(pop(l,v) == pop(l,v)))),
    '!=': ('Pushes `1` if top two inequal, else `0`', lambda l, v: l.append(int(pop(l,v) != pop(l,v)))),
    '<=': (
        'Pushes `1` if second less than or equal to top, else `0`',
        lambda l, v: l.append(int(pop(l,v,-2) <= pop(l,v)))
    ),
    '<': ('Pushes `1` if second less than top, else `0`', lambda l, v: l.append(int(pop(l,v,-2) < pop(l,v)))),
    '>': ('Pushes `1` if second greater than top, else `0`', lambda l, v: l.append(int(pop(l,v,-2) > pop(l,v)))),
    '>=': (
        'Pushes `1` if second greater than or equal to top, else `0`',
        lambda l, v: l.append(int(pop(l,v) <= pop(l,v)))
    ),

    # misc math
    'ceil': ('Ceiling function', lambda l, v: l.append(math.ceil(pop(l,v)))),
    'flr': ('Ceiling function', lambda l, v: l.append(math.floor(pop(l,v)))),
    'abs': ('Absolute value', lambda l, v: l.append(abs(pop(l,v)))),

    # statistics
    'meana': ('Pushes mean of entire stack. Keeps stack', lambda l, v: l.append(statistics.mean(l))),
    'stdva': ('Pushes standard deviation of entire stack. Keeps stack', lambda l, v: l.append(statistics.stdev(l))),
    'mean': (
        'Pops one number as n, pushes mean of top n numbers and pops them.',
        lambda l, v: l.append(statistics.mean([pop(l,v) for _ in range(pop(l,v))]))
    ),
    'stdv': (
        'Pops one number as n, pushes standard deviation of top n numbers and pops them.',
        lambda l, v: l.append(statistics.stdev([pop(l,v) for _ in range(pop(l,v))]))
    ),
    'meanstdva': (
        'Pushes standard deviation, followed by mean, of entire stack. Keeps stack.',
        lambda l, v: l.extend([
            statistics.stdev(l),
            statistics.mean(l)
        ])
    ),
    'meanstdv': (
        'Pops one number as n, pushes standard deviation, followed by mean, of top n numbers and pops them.',
        lambda l, v: l.extend([
            statistics.stdev(l[1: l[0]+1]),
            statistics.mean([pop(l,v) for _ in range(pop(l,v))])
        ])
     ),

    # strings
    'chr': ('Converts the top item of the stack to a utf-8 character', lambda l, v: l.append(chr(pop(l,v)))),
    'str': (
        'Converts the top element to its string representation. The string representation of `"a"` is `"\'a\'"`',
        lambda l, v: l.append(to_str(pop(l,v), v))
    ),
    # meta
    'eval': (
        'Evaluate the top of the stack as an expression.',
        lambda l, v: eval_rpn(pop(l,v), l, v)
    ),
    'veval': (
        'Evaluate the variable at the top of the stack',
        lambda l, v: eval_rpn(v[pop(l,v)], l, v)
    ),
    'evals': (
        'Evaluate the top of the stack as an expression, using an empty stack, '
        'then adds the new stack to the original stack. This is safer than `eval`',
        lambda l, v: l.extend(eval_rpn(pop(l,v), [], v))
    ),
    'vevals': (
        'Evaluate the variable at the top of the stack, using an empty stack, '
        'then adds the new stack to the original stack. This is safer than `veval`',
        lambda l, v: l.extend(eval_rpn(v[pop(l,v)], [], v))
    ),
    'cond': (
        'Pop 3 values, if the top value is an empty string or `0`, push the third, else push the second',
        lambda l, v: l.append((lambda i, c: c[i-1])(bool(pop(l,v,-3)), (pop(l,v,-2), pop(l,v,-1))))
    ),

    # stack operations
    'swp': ('Swap the  top two items of the stack.', lambda l, v: l.extend([pop(l,v), pop(l,v)])),
    'drp': ('Drop the top item of the stack', lambda l, v: pop(l,v)),
    'dup': ('Duplicate the top item of the stack', lambda l, v: l.extend([pop(l,v)]*2)),
    'rot': (
        'Pops the top number as n, rotates the stack from top to bottom by n places.',
        lambda l, v: [l.insert(0, pop(l,v)) for _ in range(pop(l,v) % len(l))]
    ),
    'clrs': ('Clear the stack', lambda l, v: l.clear()),


    # variables/heap
    ':=': (
        'Pops two items from the stack, '
        'the second should be a string with contents that do not resolve to a value. '
        'The second variable will resolve to the assigned value in future operations',
        lambda l, v: setvar(pop(l,v), l.pop(), v)
    ),
    'get': ("Fetch from heap by variable name.", lambda l, v: l.append(v[pop(l,v)])),
    'func': (
        'Defines a function, second from the top should be a string with space delimited names. '
        'First from top should be executable code with the names. Then assigns function to name',
        lambda l, v: setvar(makefun(pop(l,v), pop(l,v)), l.pop(), v)
    ),
    'lambda': ('Creates a function then runs it',
               lambda l, v: run_func(l, v, makefun(pop(l,v), pop(l,v)))),

    '()': ('Executes a function at the top of the stack',
           lambda l, v: run_func(l, v, pop(l, v))),

    'del': (
        'Removes a variable.',
        lambda l, v: v.pop(l.pop()[1])
    ),
    'clrv': ('Clear all variables', lambda l, v: v.clear()),

    'clra': ('Clear the stack and variables', lambda l, v: (v.clear(), l.clear())),
    'clr': ('Clear the stack and variables', lambda l, v: (v.clear(), l.clear())),
}


def eval_rpn(ops, stack, heap):
    try:
        ops = split_args(ops)
    except ValueError as e:
        raise ValueError(f"{e}")

    for n, op in enumerate(ops):
        if op in rpncalc:
            try:
                rpncalc[op][1](stack, heap)
            except IndexError:
                raise IndexError(
                    f'Stack size too small on operation {n+1}: `{op}`.'
                )
            except Exception as e:
                if not e.args: e.args = (e.__class__.__name__, )
                if len(e.args[0]) > 500:
                    m = f'{e.args[0][:500]}... [Error truncated due to length]'
                else:
                    m = f'{e}'

                raise e.__class__(m)

        else:
            try:
                val = convert(op)
            except ValueError:
                raise NameError(f'`{op}` is an invalid symbol or value')

            if isinstance(val, tuple) and val[0] == 'fn':
                run_func(stack, heap, val[1])
            else:
                stack.append(val)

    return stack


def std_complex(string) -> complex:
    try:
        return complex(string)
    except ValueError:
        return complex(string.replace('i', 'j'))


def std_str(string) -> str:
    if string.startswith('"') and string.endswith('"'):
        return string[1:-1]
    else:
        raise ValueError

def std_symbol(symbol):
    if re.match(symbol_regex, symbol):
        return 'sym', symbol

    raise ValueError

conv_list = [
    int,
    float,
    std_complex,
    std_str,
    std_symbol,
]


def convert(string):
    for func in conv_list:
        try:
            return func(string)
        except ValueError:
            pass

    raise ValueError


def to_str(value, arg, default=repr):
    if isinstance(value, complex):
        out = repr(value).replace('j', 'i')
    elif isinstance(value, tuple):
        if value[0] == 'sym' and value[1] in arg:
            out = to_str(arg[value[1]], {})

        else: out = str(value[1])

    else:
        out = default(value)

    return out

def run_func(l, v, f):
    if not (isinstance(f, tuple) and f[0] == 'fn'):
        raise ValueError("Cannot evaluate a non function")

    args, body = f[1]
    subs = {}
    body = list(body)
    for name in reversed(args):
        subs[name] = to_str(l.pop(), v)

    for n, val in enumerate(body):
        if val in subs:
            body[n] = subs[val]

    return eval_rpn(body, l, v)

def split_args(s):
    if not isinstance(s, str): return s
    args = []
    curr = ''
    quoted = False
    escaped = False
    for n, c in enumerate(s):
        if escaped:
            if c == 'n': curr += '\n'
            else: curr += c
            escaped = False
            continue

        elif c == '"':
            curr += c
            quoted = not quoted
        elif c == '\\': escaped = True
        elif quoted: curr += c
        elif c == ' ':
            args.append(curr)
            curr = ''
        else:
            curr += c

    if quoted:
        raise ValueError("You're missing a closing quote!")
    elif escaped:
        raise ValueError("There's a trailing escape character")

    if curr: args.append(curr)
    return args
