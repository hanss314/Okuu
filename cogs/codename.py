import discord
import random
from discord.ext import commands
Cog = commands.Cog

from cogs.codenames import checks
from cogs.codenames.board import Board


def to_lower(argument):
    return argument.lower()


class Codenames(Cog):
    def __init__(self, bot):
        self.bot = bot
        self.joiners = []
        self.spymasters = [None, None]
        self.players = [[], []]
        self.board = None
        self.words = []

    async def cog_check(self, ctx):
        return ctx.guild.id == 381941901462470658

    @commands.command()
    async def join(self, ctx, *flags):
        """Join the next codenames game. Usage:
        `[r|b]` - prefer Red team or Blue team
        `[m|a]` - prefer spyMaster or Agent
        """
        if ctx.author.id in list(map(lambda x:x['id'], self.joiners)):
            return await ctx.send('Unyu? You\'ve already joined')

        processed_flags = []
        opposite = {'r':'b', 'b':'r', 'm':'a', 'a':'m'}
        for flag in flags:
            flag = flag.lower()[0].replace("`", "\`")
            if flag not in opposite:
                return await ctx.send(f'Unrecognized option `{flag}`')
            elif opposite[flag] in processed_flags:
                return await ctx.send(f'`{opposite[flag]}` and `{flag}` are mutually exclusive')

            processed_flags.append(flag)

        p = {'id': ctx.author.id, 'mem': ctx.author, 'team': '', 'role': '', 'ready': False, 'kicks': set()}
        if 'b' in processed_flags: p['team'] = 'b'
        elif 'r' in processed_flags: p['team'] = 'r'
        if 's' in processed_flags: p['role'] = 's'
        elif 'a' in processed_flags: p['role'] = 'a'

        self.joiners.append(p)
        await ctx.send('You are now in the queue for the next game!')

    @commands.command()
    async def kick(self, ctx, member: discord.Member=None):
        """Vote to kick a member during joining. Provide no member to kick yourself."""
        if member is None:
            member = ctx.author.id
            mid = ctx.author.id
        else: mid = member.id
        threshold = (2*len(self.joiners))//3
        for p in self.joiners:
            if p['id'] == mid == ctx.author.id:
                self.joiners.remove(p)
                await ctx.send('You have been removed from the waitlist')
                await self.attempt_start(ctx)
                return
            elif p['id'] == mid:
                p['kicks'].add(ctx.author.id)
            else:
                continue

            if len(p['kicks']) > threshold:
                self.joiners.remove(p)
                await ctx.send(f'{member} has been removed from waitist!')
                await self.attempt_start(ctx)
                return
            else:
                return await ctx.send(
                    f'You have voted to kick {member} from the waitlist. '
                    f'{len(p["kicks"])}/{threshold+1} required votes.'
                )

        return await ctx.send(f"{member} is not in the waitlist.")

    @staticmethod
    def want_sm(l):
        lvls = [[], []]
        for p in l:
            if p['role'] == 's': lvls[1].append(p)
            elif p['role'] == '': lvls[0].append(p)

        return lvls

    def force_balance(self, a, b):
        forced = []
        while len(a) > len(b) + 1:
            aroles = self.want_sm(a)
            broles = self.want_sm(b)
            if len(aroles[1]) == 0 and len(broles[1]) >= 2:
                p = random.choice(broles[1])
                b.remove(p)
            elif len(aroles[0]) == 0 and len(broles[0])+len(broles[1]) >= 2:
                p = random.choice(broles[0])
                b.remove(p)

            p = a.pop(random.randint(0, len(a) - 1))
            b.append(p)
            forced.append(p['mem'])

        return forced

    async def balance_teams(self, reds, blues, neut):
        c = self.bot.get_channel(self.bot.config['chans']['main'])
        red_assigns = []
        blu_assigns = []
        for p in neut:
            if len(reds) > len(blues):
                blu_assigns.append(p['mem'])
                blues.append(p)
            elif len(blues) > len(reds):
                red_assigns.append(p['mem'])
                reds.append(p)
            elif random.choice([True, False]):
                blu_assigns.append(p['mem'])
                blues.append(p)
            else:
                red_assigns.append(p['mem'])
                reds.append(p)

        if red_assigns:
            await c.send(f'{", ".join(map(str, red_assigns))} have been assigned to red!')
        if blu_assigns:
            await c.send(f'{", ".join(map(str, blu_assigns))} have been assigned to blue!')

        forced = self.force_balance(blues, reds)
        if forced:
            await c.send(f'{", ".join(map(str, forced))} have been switched to red for balance!')

        forced = self.force_balance(reds, blues)
        if forced:
            await c.send(f'{", ".join(map(str, forced))} have been switched to blue for balance!')

    def choose_sm(self, team):
        l = [t for t in team if t['role'] == 's']
        if not l:
            l = [t for t in team if t['role'] != 'a']
        if not l:
            l = team

        p = random.choice(l)
        team.remove(p)
        return p

    async def attempt_start(self, ctx):
        if len(self.joiners) < 4:
            await ctx.send('We need at least 4 people to start!')
            return False
        c = self.bot.get_channel(self.bot.config['chans']['main'])
        reds = []
        blues = []
        neut = []
        for p in self.joiners:
            if not p['ready']: return False
            if p['team'] == 'r': reds.append(p)
            elif p['team'] == 'b': blues.append(p)
            else: neut.append(p)

        await c.send('We are ready! Making teams!')
        await self.balance_teams(reds, blues, neut)
        red_sm = self.choose_sm(reds)
        await c.send(f'{red_sm["mem"]} is the red spymaster!')
        blue_sm = self.choose_sm(blues)
        await c.send(f'{blue_sm["mem"]} is the blue spymaster!')
        self.board = Board(words=list(set(self.words)))
        self.joiners = []
        self.words = []
        self.players = [[s['mem'] for s in reds], [s['mem'] for s in blues]]
        self.spymasters = [red_sm['mem'], blue_sm['mem']]
        await self.set_perms()
        await c.send(f'**Red SM:** {self.spymasters[0].mention}')
        await c.send(f'**Team Red:** {"".join(s.mention for s in self.players[0])}')
        await c.send(f'**Blue SM:** {self.spymasters[1].mention}')
        await c.send(f'**Team Blue:** {"".join(s.mention for s in self.players[1])}')

        chan = ctx.bot.get_channel(self.bot.config['chans']['sm'])
        self.board.draw(unhidden=True).save('bot_data/board.png')
        await chan.send(file=discord.File(open('bot_data/board.png', 'rb')))

        self.board.draw().save('bot_data/board.png')
        img = discord.File(open('bot_data/board.png', 'rb'))
        await c.send(f'{self.spymasters[0].mention} make a hint!', file=img)

        return True

    async def set_perms(self):
        sm = self.bot.get_channel(self.bot.config['chans']['sm'])
        red = self.bot.get_channel(self.bot.config['chans']['red'])
        blue = self.bot.get_channel(self.bot.config['chans']['blue'])
        allow = discord.PermissionOverwrite(read_messages=True, send_messages=True)
        await sm.set_permissions(self.spymasters[0], overwrite=allow)
        await sm.set_permissions(self.spymasters[1], overwrite=allow)
        for p in self.players[0]:
            await red.set_permissions(p, overwrite=allow)

        for p in self.players[1]:
            await blue.set_permissions(p, overwrite=allow)

    async def rem_perms(self):
        sm = self.bot.get_channel(self.bot.config['chans']['sm'])
        red = self.bot.get_channel(self.bot.config['chans']['red'])
        blue = self.bot.get_channel(self.bot.config['chans']['blue'])
        await sm.set_permissions(self.spymasters[0], overwrite=None)
        await sm.set_permissions(self.spymasters[1], overwrite=None)
        for p in self.players[0]:
            await red.set_permissions(p, overwrite=None)

        for p in self.players[1]:
            await blue.set_permissions(p, overwrite=None)


    @commands.command()
    async def start(self, ctx):
        """Vote to start"""
        if self.board is not None:
            return await ctx.send('A game is happening right now!')

        for p in self.joiners:
            if p['id'] == ctx.author.id:
                p['ready'] = True
                if not await self.attempt_start(ctx):
                    waiting = ', '.join(str(m['mem']) for m in self.joiners if not m['ready'])
                    await ctx.send(f'You are ready! Waiting upon {waiting}')

                return

        return await ctx.send(f'You are not in the waitlist! Use `u!join` to waitlist!')


    @commands.command()
    async def add_words(self, ctx, *words):
        """Add some words"""
        self.words.extend(words)
        return await ctx.send("Words added")

    @commands.command()
    async def clear_words(self, ctx):
        """Remove all the words and use defaults"""
        self.words.clear()
        return await ctx.send("Using default words")

    @commands.command()
    async def hint(self, ctx, word, num):
        """Give a hint"""
        turn = self.board.get_turn()
        sm = self.spymasters[turn]
        if ctx.author.id != sm.id:
            return await ctx.send(
                'Foreign substance detected! ...what do I do again? Whatever, I\'ll just ~~exterminate~~ ignore it.',
                delete_after = 10
            )

        try: await ctx.message.delete()
        except discord.Forbidden: pass
        if self.board.max_moves != 0:
            return await ctx.send('You\'ve already made a hint!', delete_after=10)
        try: 
            n = int(num)
        except ValueError:
            n = 'unlimited'
            self.board.set_hint_number(100000)
        else: 
            if n == 0:
                self.board.set_hint_number(100000)
            elif n < 0:
                return await ctx.send('Just... No.')
            else:
                self.board.set_hint_number(n)

        self.board.hint_word = f'"{word}: {n}"'
        c = self.bot.get_channel(self.bot.config['chans']['main'])
        await c.send(
            f'{"".join(s.mention for s in self.players[turn])} The hint is "{word}: {n}"'
        )
        
    @commands.command(aliases=['submit'])
    async def guess(self, ctx, word):
        """Guess a square"""
        turn = self.board.get_turn()
        team = self.players[turn]
        if ctx.author.id not in list(map(lambda x:x.id, team)):
            return await ctx.send(
                'Foreign substance detected! ...what do I do again? Whatever, I\'ll just ~~exterminate~~ ignore it.',
                delete_after = 10
            )
        if self.board.max_moves == 0:
            return await ctx.send('There is no hint!', delete_after=10)
        move = self.board.move_count
        result = self.board.do_move(word)
        c = self.bot.get_channel(self.bot.config['chans']['main'])
        if result == -1: return
        await c.send('{}. {}'.format(move, word))
        self.board.draw().save('bot_data/board.png')
        img = discord.File(open('bot_data/board.png', 'rb'))
        await c.send(file=img)
        if result is not None:
            await c.send(f'{"".join(s.mention for s in self.players[1-turn])} You win! Good job!')
            self.board.draw(unhidden=True).save('bot_data/board.png')
            img = discord.File(open('bot_data/board.png', 'rb'))
            await c.send(file=img)
            await self.rem_perms()
            self.spymasters = [None, None]
            self.players = [[], []]
            self.board = None
            return
        
        if self.board.max_moves == 0: 
            await c.send(f'{self.spymasters[self.board.get_turn()].mention} '
                         f'It\'s your turn to make a hint!')
        
    @commands.command(aliases=['pass'])
    async def skip(self, ctx):
        """Skip your team's turn"""
        turn = self.board.get_turn()
        team = self.players[turn]
        if ctx.author.id not in list(map(lambda x: x.id, team)):
            return await ctx.send(
                'Foreign substance detected! ...what do I do again? Whatever, I\'ll just ~~exterminate~~ ignore it.',
                delete_after=10
            )
        self.board.switch_turn()
        await ctx.send('Turn skipped.')
        c = self.bot.get_channel(self.bot.config['chans']['main'])
        await c.send(f'{self.spymasters[self.board.get_turn()].mention} '
                     f'It\'s your turn to make a hint!')

    @commands.command()
    async def get_hint(self, ctx):
        """Get the current hint"""
        turn = self.board.get_turn()
        team = self.players[turn]
        if ctx.author.id not in list(map(lambda x: x.id, team)):
            return await ctx.send(
                'Foreign substance detected! ...what do I do again? Whatever, I\'ll just ~~exterminate~~ ignore it.',
                delete_after=10
            )
        try: await ctx.send(self.board.hint_word)
        except AttributeError:
            await ctx.send('There is no hint!')
        
    @commands.command(name='board')
    @commands.cooldown(1, 15, commands.BucketType.guild)
    async def get_board(self, ctx):
        """Show the board"""
        if self.board is None: await ctx.send('There is no board.')
        if ctx.channel.id == ctx.bot.config['chans']['sm']:
            self.board.draw(True).save('bot_data/board.png')
        else:
            self.board.draw().save('bot_data/board.png')
        img = discord.File(open('bot_data/board.png', 'rb'))
        await ctx.send(file=img)

    async def setup(self, ctx, *words: to_lower):
        """Setup a new board"""
        self.board = Board(words=list(set(words)))
        await ctx.send('Setup a new board!')
        chan = ctx.bot.get_channel(ctx.bot.config['revealed'])
        self.board.draw(unhidden=True).save('bot_data/board.png')
        await chan.send(file=discord.File(open('bot_data/board.png', 'rb')))
        for member in ctx.guild.members:
            for role in member.roles:
                if role.id in (ctx.bot.roles['red_sm'], ctx.bot.roles['red_sm']):
                    try:
                        await member.send(file=discord.File(open('bot_data/board.png', 'rb')))
                    except discord.Forbidden:
                        pass

        self.board.draw().save('bot_data/board.png')
        img = discord.File(open('bot_data/board.png', 'rb'))
        chan = ctx.bot.get_channel(ctx.bot.config['channel'])
        await chan.send(file=img)
        await chan.send('<@&{}> It\'s your turn to make a hint!'.format(ctx.bot.roles['red_sm']))



def setup(bot):
    bot.add_cog(Codenames(bot))
