import discord
import asyncio
import re
import gspread
import youtube_dl
import functools

from oauth2client.service_account import ServiceAccountCredentials
from discord.ext import commands

if True:
    GUILD = 381941901462470658
    MOOSIC = 383041784495734785
else:
    GUILD = 297811083308171264
    MOOSIC = 297811083308171264

YT_RE = re.compile(r'((http(s)?://)?(www\.)?youtu(\.be/|be\.com/watch\?v=)([a-zA-Z0-9_\-]{11}))')


def format_time(t):
    seconds = t%60
    minutes = t//60
    hours = minutes//60
    minutes %= 60
    return f'{hours}:{minutes:02d}:{seconds:02d}.000'

class Moosic(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        scope = ['https://spreadsheets.google.com/feeds']
        creds = ServiceAccountCredentials.from_json_keyfile_name('config/sheets.json', scope)
        self.gc = gspread.authorize(creds)
        self.sheet = self.gc.open_by_url(bot.config['moosic']['url'])
        self.users = bot.config['moosic']['users']
        self.ratings = self.sheet.get_worksheet(1)

    async def add_to_sheet(self, url):
        base= url[-1]
        ratings = self.ratings
        call = functools.partial(ratings.findall, re.compile('.*'+base+'.*'))

        if await self.bot.loop.run_in_executor(None, call): return

        with youtube_dl.YoutubeDL({}) as ydl:
            call = functools.partial(ydl.extract_info, base, download=False)
            info = await self.bot.loop.run_in_executor(None, call)

        title = info['title']
        time = format_time(info['duration'])

        row = len(ratings.col_values(1)) + 1
        call = functools.partial(ratings.update, f'A{row}:C{row}', [[title, url[0], time]], value_input_option='USER_ENTERED')
        await self.bot.loop.run_in_executor(None, call)
        #ratings.update(f'C{row}', time, format={"numberFormat": {"type": "TIME", "pattern": "hh:mm:ss"}})

    @commands.Cog.listener()
    async def on_message(self, message):
        if message.channel.id != MOOSIC: return
        urls = YT_RE.findall(message.content)
        for url in urls:
            await self.add_to_sheet(url)

    async def rate_row(self, ctx, row, rating):
        col = 9 + self.users.index(ctx.author.id)
        await self.bot.loop.run_in_executor(None, self.ratings.update_cell, row, col, rating)
        message = await ctx.send(f'{ctx.author.mention} {self.ratings.cell(row, 1).value} rated as {rating}')
        if message.channel.id == MOOSIC:
            await asyncio.sleep(10)
            await message.delete()

    @commands.command(aliases=['rl'])
    async def rate_last(self, ctx, rating, row: int=0):
        """Rate a song. You can put anything for the rating,
        but please keep numbers between 0 and 10"""
        if ctx.author.id not in self.users:
            return await ctx.send(
                'You have not been set up on the sheet. '
                'Contact hanss314 to get you setup.'
            )
        if row < 0: return await ctx.send('You cannot rate a future song.', delete_after=10)
        last = len(self.ratings.col_values(1)) - row
        if last < 3: return await ctx.send('Please do not overwrite the header.', delete_after=10)
        await self.rate_row(ctx, last, rating)

    @commands.command(aliases=['madd'])
    async def moosic_add(self, ctx, title, link, time):
        """Add a song to the sheet. Please use responsibily"""
        row = len(self.ratings.col_values(1))+1
        call = functools.partial(self.ratings.update, f'A{row}:C{row}', [[title, link, time]], value_input_option='USER_ENTERED')
        await self.bot.loop.run_in_executor(None, call)
        message = await ctx.send('Sheet updated.')
        if message.channel.id == MOOSIC:
            await asyncio.sleep(10)
            await message.delete()
    
    @commands.command(aliases=['mlast'])
    async def moosic_last(self, ctx, row: int=0):
        """Get last song on a sheet. Please use responsibily"""
        if row < 0: return await ctx.send('You cannot rate a future song.', delete_after=10)
        last = len(self.ratings.col_values(1)) - row
        if last < 3: return await ctx.send('Please do not overwrite the header.', delete_after=10)
        row = await self.bot.loop.run_in_executor(None, self.ratings.row_values, last)

        message = await ctx.send(row[0])
        if message.channel.id == MOOSIC:
            await asyncio.sleep(10)
            await message.delete()

    async def cog_check(self, ctx):
        return ctx.guild is not None and ctx.guild.id == GUILD

def setup(bot):
    bot.add_cog(Moosic(bot))
