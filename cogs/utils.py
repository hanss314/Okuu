import asyncio
import discord
import time
import os
import matplotlib as mpl
import inspect
import yaml as pyyaml
import json
import functools
import datetime as dt

import pytz
from geopy.adapters import AioHTTPAdapter
from geopy.geocoders import Nominatim
from timezonefinder import TimezoneFinder
from .util.helpers import cacheable

mpl.rcParams['text.usetex'] = True
mpl.use('agg')
from matplotlib import pyplot as plt

from os import path
from ruamel import yaml
from .util import sudoku, comp_parser
from .util.rpn import rpncalc, to_str, help_messages, eval_rpn
from .util import arith
from discord.ext import commands
from .util.volumes import volume, volume_units
from subprocess import PIPE
from PIL import Image
from .util.cas import parse_cas
from .util import cockatrice
Cog = commands.Cog


CACHE_SIZE = 2048

class Utils(Cog):

    def __init__(self, bot):
        import importlib
        from .util import rpn, arith
        importlib.reload(rpn)
        importlib.reload(arith)
        self.slide_positions = [0.0, 0.0]
        self.bot = bot
        self.duck_queue = []
        self.remind_lock = []
        self.duck_answers = {}

        self.nom = Nominatim(user_agent="utsuho-bot/0.0.1", adapter_factory=AioHTTPAdapter)
        self.tf = TimezoneFinder()
        self.location_cache = []

        if not path.isfile('bot_data/stacks.yml'):
            with open('bot_data/stacks.yml', 'w+') as stacks:
                yaml.dump({}, stacks)

        if not path.isfile('bot_data/latexes.yml'):
            with open('bot_data/latexes.yml', 'w+') as texes:
                yaml.dump({}, texes)

        with open('bot_data/stacks.yml', 'r') as stacks:
            self.stacks = pyyaml.load(stacks)

        with open('bot_data/latexes.yml', 'r') as texes:
            self.texes = yaml.load(texes)

    @staticmethod
    def do_latex(text):
        filename = 'bot_data/latex_' + str(round(time.time() - 0x5B898860)) + '.png'
        plt.clf()
        fig, ax = plt.subplots()
        plt.rc('text', usetex=True)
        plt.rc('text.latex', preamble=
            r'\usepackage{amsmath}' + '\n'
            r'\usepackage{pxfonts}' + '\n'
        )
        ax.set_axis_off()
        fig.patch.set_visible(False)
        fig.text(0, 0.5, r'\[ ' + text.strip('`') + r' \]', fontsize=14)
        try:
            fig.savefig(filename)
        except (RuntimeError, UnicodeEncodeError) as e:
            return False, filename
        image = Image.open(filename)
        thresholded = [(0,) * 4 if item[3] == 0 else item for item in image.getdata()]
        image.putdata(thresholded)
        image = image.crop(image.getbbox())

        background = Image.new('RGBA', (image.width + 10, image.height + 10), color=(200, 200, 200, 200))
        background.paste(image, (5, 5), image)
        background.save(filename)
        return True, filename

    @Cog.listener()
    async def on_raw_message_edit(self, payload):
        message_id = payload.message_id
        if message_id in self.texes:
            if time.time() - self.texes[message_id]['time'] > 600:
                del self.texes[message_id]
            else:
                data = self.texes[message_id]
                channel = self.bot.get_channel(data['channel'])
                command = await channel.fetch_message(message_id)
                sent = await channel.fetch_message(data['id'])
                command = command.content.split(' ')
                if data['prefix'] in command[0]:
                    command = command[1:]
                command = ' '.join(command)
                timeout = 15
                success = False
                future = self.bot.loop.run_in_executor(None, self.do_latex, command)
                try:
                    success, filename = await asyncio.wait_for(future, timeout, loop=self.bot.loop)
                except asyncio.futures.TimeoutError:
                    filename = None

                if filename is None: return
                await sent.delete()
                if success and os.path.exists(filename):
                    sent = await channel.send(file=discord.File(filename))
                else:
                    sent = await channel.send('That doesn\'t look right.')

                data['time'] = time.time()
                data['id'] = sent.id

                if os.path.exists(filename):
                    os.remove(filename)

            for m_id, data in list(self.texes.items()):
                if time.time() - data['time'] > 600:
                    del self.texes[m_id]

            with open('bot_data/latexes.yml', 'w') as texes:
                yaml.dump(self.texes, texes)

    @Cog.listener()
    async def on_member_update(self, before, after):
        BOTS = {
            233034619069530113: 240995021208289280,
            444884890878738442: 140564059417346049,
            329431169386938371: 233320738512306177, 309147665852137473: 240995021208289280,
            353957562200227840: 186553034439000064, 666802713689980928: 248953835899322370,
            338683671664001024: 161508165672763392, #416693134412611586: 136611352692129792,
            382925349144494080: 137001076284063744,
            # 322059317689974799: 141294044671246337, 
        }
        if before.id not in BOTS or before.guild.id != 381941901462470658 or \
                before.status == discord.Status.offline or after.status != discord.Status.offline:
            return

        if before.id in self.remind_lock: return
        self.remind_lock.append(before.id)

        await asyncio.sleep(60)
        self.remind_lock.remove(before.id)
        if after.guild.get_member(after.id).status != discord.Status.offline: return
        channel = self.bot.get_channel(381950042770243584)
        await channel.send(f'<@{BOTS[before.id]}>, <@{before.id}> just went down!')


    @commands.group(invoke_without_command=True)
    async def rpn(self, ctx, *, ops):
        """Use an RPN calculator"""
        stack, heap = self.stacks[ctx.author.id]
        stack, heap = list(stack), dict(heap)
        loop = asyncio.get_event_loop()
        task = asyncio.wait_for(
            loop.run_in_executor(None, eval_rpn, ops, stack, heap), 2
        )
        try:
            stack = await task
        except asyncio.TimeoutError:
            return await ctx.send(f'Operation {n+1}: `{op}` timed out. Aborting.')
        except Exception as e:
            # raise e
            return await ctx.send(f"Unyu? {e}")

        if stack == -1:
            return await ctx.send(heap)

        while len(heap) > 256:
            heap.popitem()

        self.stacks[ctx.author.id] = (stack[-256:], heap)
        await ctx.invoke(self.show_stack)

    @rpn.command(name='help')
    async def rpn_help(self, ctx, op=None):
        """Get a list of all operations"""
        if op is None:
            await ctx.send(
                f'Available operations: ```{", ".join(x for x in rpncalc.keys())}```\n'
                'All other operations will attempt to evaluate the operator in the order '
                '`int, float, complex, string` and push the value to the stack.\n'
                f'See `{ctx.prefix}rpn help string` for help on strings, '
                f'and `{ctx.prefix}rpn help <op>` for help on an operator.'
            )

        else:
            help_text = rpncalc.get(op)
            if help_text is None:
                help_text = [help_messages.get(op)]

            if help_text[0] is None:
                return await ctx.send('Unyu? That\'s not an operator')

            help_text = help_text[0]
            await ctx.send(f'Help for `{op}`: \n\n{help_text}')

    @rpn.group(name='infix', aliases=['inf'], invoke_without_command=True)
    async def rpn_arith(self, ctx, *, expr):
        """Use infix mode see `inf help` for available features"""
        expr = expr.strip()
        try:
            ops, l = arith.BFN().parse(expr)
        except arith.ParseException:
            return await ctx.send("Unyu? That's not a valid expression.")

        if l != len(expr):
            return await ctx.send("Unyu? That's not a valid expression.")

        await ctx.invoke(self.rpn, ops=ops)

    @rpn_arith.command(name="dry", aliases=['dryrun'])
    async def rpn_arith_dry(self, ctx, *, expr):
        """Do a dry run to see the rpn code that will be run"""
        expr = expr.strip()
        try:
            ops, l = arith.BFN().parse(expr)
        except arith.ParseException:
            return await ctx.send("Unyu? That's not a valid expression.")

        if l != len(expr):
            return await ctx.send("Unyu? That's not a valid expression.")

        await ctx.send(
            f"Your expression will be converted to `{' '.join(ops)}`"
        )

    @rpn_arith.command(name="help")
    async def rpn_arith_help(self, ctx):
        """View features available in infix mode"""
        await ctx.send(
            f"See `{ctx.prefix} rpn help` for usage"
            f"Constants: ```{', '.join(arith.consts)}```\n"
            f"Operators: ```{', '.join(map(lambda x: x[0], arith.opn))}```\n"
            f"Functions: ```{', '.join(arith.fn)}```"
        )

    @rpn.command(name='stack')
    async def show_stack(self, ctx):
        """View your stack"""
        if len(self.stacks[ctx.author.id][0]) == 0:
            return await ctx.send('Your stack is empty.')

        heap = self.stacks[ctx.author.id][1]
        await ctx.send(', '.join(to_str(x, heap) for x in self.stacks[ctx.author.id][0]))

    @rpn.command(name='variables', aliases=['vars'])
    async def show_variables(self, ctx):
        """View your stack"""
        if len(self.stacks[ctx.author.id][1]) == 0:
            return await ctx.send('You have no defined variables.')


        heap = {}
        await ctx.send('; '.join(
            f'{to_str(k, heap, str)} = {to_str(v, heap)}' for k, v in self.stacks[ctx.author.id][1].items()
        ))

    @rpn.before_invoke
    @show_stack.before_invoke
    @show_variables.before_invoke
    async def check_exists(self, ctx):
        if ctx.author.id not in self.stacks:
            self.stacks[ctx.author.id] = ([], {})

        elif type(self.stacks[ctx.author.id]) == list:
            self.stacks[ctx.author.id] = (self.stacks[ctx.author.id], {})

    @rpn.after_invoke
    async def save_stack(self, _):
        with open('bot_data/stacks.yml', 'w') as stacks:
            yaml.dump(self.stacks, stacks)

    @commands.group(invoke_without_command=True, aliases=['duck'])
    async def rubber_duck(self, ctx):
        """A rubber duck"""
        await ctx.send('Rubber ducks says: "Quack!"')

    @rubber_duck.command(name='ask')
    async def duck_ask(self, ctx, *, question):
        """Ask the rubber duck a question"""
        self.duck_queue.append((ctx.author.id, question))
        await ctx.send('Question noted. Please wait warmly.')

    @rubber_duck.command(name='answer')
    async def duck_answer(self, ctx, *, answer=None):
        """Help answer a question from the rubber duck"""
        if ctx.author.id not in self.duck_answers:
            for n, q in enumerate(self.duck_queue):
                if q[0] != ctx.author.id:
                    question = self.duck_queue.pop(n)
                    break
            else:
                return await ctx.send('No questions right now, sorry!')

            self.duck_answers[ctx.author.id] = question
            await ctx.send(f'{ctx.author.mention}\n{question[1]}')
            await ctx.send(f'Use `{ctx.prefix}duck answer` again to answer the question')

        else:
            if answer is None:
                return await ctx.send('Please provide an answer!')
            question = self.duck_answers[ctx.author.id]
            user = self.bot.get_user(question[0])
            del self.duck_answers[ctx.author.id]
            await ctx.send(f'Thank you!')
            await user.send(f'You asked: {question[1]}\n\nA: {answer}')


    @commands.command()
    async def sudoku(self, ctx, *, puzzle):
        """
        Solve a sudoku.
        Acceptable non-blanks are non whitespace characters.
        Acceptable blanks are alphanumerics not 1-9.
        Other characters are ignored
        """
        def to_int(num):
            try:
                return int(num)
            except ValueError:
                return 0

        args = [c for c in puzzle if c not in ' \n']
        rows = []
        while args:
            row, args = args[:9], args[9:]
            rows.append([to_int(c) for c in row])

        if len(rows[-1]) < 9 or len(rows) < 9:
            return await ctx.send('Please fill in everything')
        loop = asyncio.get_event_loop()
        puzzle = sudoku.Sudoku(rows)
        for x in range(9):
            for y in range(9):
                v = puzzle.get(x, y)
                if v is not None and v not in puzzle.possible(x, y):
                    return await ctx.send('Unsolvable sudoku.')

        try:
            solved = await asyncio.wait_for(loop.run_in_executor(None, sudoku.solve, puzzle), 1)
        except asyncio.TimeoutError:
            return await ctx.send('Are you sure you entered a proper sudoku?')

        if solved is not None:
            await ctx.send(f'```{str(solved)}```')
        else:
            await ctx.send('Unsolvable sudoku.')

    @commands.command(aliases=['tex'])
    async def latex(self, ctx, *, text):
        """Render a LaTeX equation"""
        timeout = 15
        success = False
        async with ctx.typing():
            future = ctx.bot.loop.run_in_executor(None, self.do_latex, text)

            try:
                success, filename = await asyncio.wait_for(future, timeout, loop=ctx.bot.loop)
            except asyncio.futures.TimeoutError:
                filename = None

        if filename is None:
            return await ctx.send('Took too long; giving up.')

        if success and os.path.exists(filename):
            sent = await ctx.send(file=discord.File(filename))
        else:
            sent = await ctx.send('That doesn\'t look right.')

        self.texes[ctx.message.id] = {
            'time':time.time(), 'channel': ctx.channel.id,
            'id': sent.id, 'prefix': ctx.prefix
        }
        if os.path.exists(filename):
            os.remove(filename)

    @commands.command()
    async def cc(self, ctx, *, value: volume):
        """Converts a volume to ccs"""
        units = volume_units.get(value[1])
        if units == None: return await ctx.send('Unyu? I don\'t know those units.')
        vol = value[0] * units[1]
        vol = format(vol, '.4f').rstrip('0').rstrip('.')
        await ctx.send(f'{value[0]} {units[0]} is equivalent to {vol} ccs')


    @commands.command(aliases=['mass', 'molarmass'])
    async def molar_mass(self, ctx, *, compound):
        """
        Get the molar mass of a compound.
        Charges are not supported
        """
        try:
            composition, charge = comp_parser.get_mass(
                comp_parser.parse_comp(compound)
            )
        except ValueError:
            return await ctx.send('Are your brackets balanced?')
        except KeyError:
            return await ctx.send('Invalid element')
        else:
            await ctx.send(
                f'The molar mass of `{compound}` is {composition:.3f}g/mol. '
                f'The charge is {charge:+d}.'
            )

    @commands.command()
    async def source(self, ctx, *, command: str = None):
        """Displays the source for a command."""
        source_url = 'https://gitlab.com/hanss314/Okuu/'
        if command is None:
            return await ctx.send(source_url)

        obj = self.bot.get_command(command.replace('.', ' '))
        if obj is None:
            return await ctx.send('Could not find command.')

        # since we found the command we're looking for, presumably anyway, let's
        # try to access the code itself
        src = obj.callback.__code__
        lines, firstlineno = inspect.getsourcelines(src)
        if not obj.callback.__module__.startswith('discord'):
            # not a built-in command
            location = os.path.relpath(src.co_filename).replace('\\', '/')
            source_url = 'https://gitlab.com/hanss314/Okuu/tree/master'
        else:
            location = obj.callback.__module__.replace('.', '/') + '.py'
            source_url = 'https://github.com/Rapptz/discord.py/blob/rewrite'

        final_url = f'<{source_url}/{location}#L{firstlineno}-{firstlineno + len(lines) - 1}>'
        await ctx.send(final_url)

    @commands.command()
    async def factor(self, ctx, number: int):
        """Factor a number using GNU factor"""
        sup = '⁰¹²³⁴⁵⁶⁷⁸⁹'
        def getsup(n:int):
            if n == 1: return ''
            return ''.join(sup[int(c)] for c in str(n))

        if number <= 0: return await ctx.send("I can only factor positive numbers!")
        if number == 1: return await ctx.send('1 has no factors')

        proc = await asyncio.create_subprocess_exec('cogs/util/factor', str(number), stdout=PIPE)
        timeout = False
        try:
            code = await asyncio.wait_for(proc.wait(), timeout=2)
        except asyncio.TimeoutError:
            proc.terminate()
            timeout = True

        else:
            if code != 0: return await ctx.send('Something went wrong.')

        output = await proc.communicate()
        values = output[0].decode('utf8').split(':')[1].strip().split(' ')
        values = list(filter(lambda x:x, values))
        if timeout:
            total = 1
            for val in map(int, values): total *= val
            values.append(str(number//total))

        s = ''
        curr = values[0]
        count = 1
        for c in values[1:]:
            if c == curr: count += 1
            else:
                s += str(curr) + getsup(count) + ' * '
                curr = c
                count = 1

        s += str(curr) + getsup(count)
        s = f'`{s}`'
        if not s: s = f'{number} has no factors!'
        return await ctx.send(s)

    @commands.command()
    async def bc(self, ctx, *, expression):
        """Infix calculator with support for functions.
        Makes a call to GNU bc
        https://www.gnu.org/software/bc/manual/html_mono/bc.html
        """
        with open('config/preamble.bc', 'rb') as pfile:
            preamble = pfile.read()

        expression = expression.strip('`').encode('utf8').lstrip(b'\n')+b'\n'
        expression = preamble + b'\n' + expression
        proc = await asyncio.create_subprocess_exec(
            'bc', '-l', stdin=PIPE, stdout=PIPE, stderr=PIPE
        )
        try:
            out, err = await asyncio.wait_for(proc.communicate(input=expression), timeout=5)
        except asyncio.TimeoutError:
            proc.terminate()
            return await ctx.send('Evaluation timed out.')

        print(out, err)

        if err: return await ctx.send(err.decode('utf8'))
        return await ctx.send(out.decode('utf8').replace('\\\n', ''))

    @commands.command()
    async def dc(self, ctx, *, expression):
        """Postfix calculator with support for functions.
        Makes a call to GNU dc
        https://www.gnu.org/software/bc/manual/dc-1.05/html_mono/dc.html
        """
        expression = expression.strip('`').encode('utf8').lstrip(b'\n')+b'\n'
        proc = await asyncio.create_subprocess_exec(
            'dc', stdin=PIPE, stdout=PIPE, stderr=PIPE
        )
        try:
            out, err = await asyncio.wait_for(proc.communicate(input=expression), timeout=5)
        except asyncio.TimeoutError:
            proc.terminate()
            return await ctx.send('Evaluation timed out.')

        if err: return await ctx.send(err.decode('utf8'))
        return await ctx.send(out.decode('utf8'))

    @commands.command(aliases=['sr', 'slide'])
    async def sliderule(self, ctx, object=None, amount: float=0.0):
        """Use the slide rule.
        Object should be `c` for the cursor and `s` for the slider.
        By default, the slide rule will be posted.
        Amount is how many pixels to move said object by, defaults to 0
        """
        IMAGEDIR = 'images/sliderule/'
        if not object: object = 'a'
        object = object.lower()[0]
        if object == 'c': self.slide_positions[1] += amount
        elif object == 's': self.slide_positions[0] += amount
        for i in range(2):
            self.slide_positions[i] = min(1500, max(-1500, self.slide_positions[i]))

        slidery = 98
        imgheight = 400
        imgwidth = 1500 + \
                   max([abs(x) for x in self.slide_positions if x < 0]+[0]) + \
                   max([x for x in self.slide_positions if x > 0]+[0])

        imgwidth = int(imgwidth)

        leftmost = min(0, *self.slide_positions) - 50
        img = Image.new('RGBA', (imgwidth, imgheight))

        casing = Image.open(IMAGEDIR+'casing.png')
        img.paste(casing, (int(-leftmost), (imgheight-casing.height)//2), casing)
        margin = int((imgheight-casing.height)/2)
        casing.close()

        slider = Image.open(IMAGEDIR + 'slider.png').convert('RGBA')
        img.paste(slider, (int(self.slide_positions[0] - leftmost), slidery+margin), slider)
        slider.close()

        cursor = Image.open(IMAGEDIR + 'cursor.png')
        img.paste(cursor, (int(self.slide_positions[1] - leftmost), margin//2), cursor)
        cursor.close()
        
        img.save('bot_data/sliderule.png')
        img.close()
        await ctx.send(file=discord.File('bot_data/sliderule.png'))

    @commands.command()
    async def cas(self, ctx, *, roman):
        """Romanization to Canadian Aboriginal Syllabics"""
        try:
            await ctx.send(parse_cas(roman.lower()))
        except KeyError:
            await ctx.send('Invalid romanization.')

    @staticmethod
    def clean_codeblocks(s):
        s = s.strip()
        if s.startswith('```') and s.endswith('```'):
            s = s[s.index('\n')+1:-3]
        return s.strip()

    @commands.group(name='tag', invoke_without_command=True)
    @commands.guild_only()
    async def cockatrice_tag(self, ctx, tag=None, *args):
        """Run a tag"""
        if tag is None: return await ctx.send(f'Use `{ctx.prefix}help tag` for usage.')
        archive = f'bot_data/tags/{ctx.guild.id}/{tag}.json'
        if not os.path.isfile(archive): return await ctx.send('Tag not found')
        with open(archive, 'r') as f:
            data = json.load(f)

        await ctx.send(await cockatrice.run_program(data['code'], (ctx, args, data['vars'])))
        with open(archive, 'w') as f:
            json.dump(data, f)

    @cockatrice_tag.command(name='run', rest_is_raw=True)
    async def tag_run(self, ctx, *, s):
        """Run a cockatrice program"""
        s = self.clean_codeblocks(s)
        await ctx.send(await cockatrice.run_program(s, (ctx, tuple(), {})))

    @cockatrice_tag.command(name='new', rest_is_raw=True)
    @commands.guild_only()
    async def tag_new(self, ctx, name, *, content):
        """Create a tag for a guild"""
        folder = f'bot_data/tags/{ctx.guild.id}'
        if not os.path.isdir(folder): os.mkdir(folder)
        archive = f'{folder}/{name}.json'
        if os.path.isfile(archive): return await ctx.send('This tag already exists')
        content = self.clean_codeblocks(content)
        data = {'owner': ctx.author.id, 'name': name, 'vars': {}, 'code': content}
        with open(archive, 'w') as f: json.dump(data, f)
        await ctx.send('Tag created!')

    @cockatrice_tag.command(name='edit', rest_is_raw=True)
    @commands.guild_only()
    async def tag_edit(self, ctx, name, *, content):
        """Edit a tag"""
        archive = f'bot_data/tags/{ctx.guild.id}/{name}.json'
        if not os.path.isfile(archive): return await ctx.send('Tag not found')
        with open(archive, 'r') as f: data = json.load(f)
        if ctx.author.id != data['owner']: return await ctx.send('You did not make this tag.')
        data['code'] = self.clean_codeblocks(content)
        with open(archive, 'w') as f: json.dump(data, f)
        await ctx.send('Tag edited!')

    @cockatrice_tag.command(name='delete', aliases=['del', 'remove', 'rm', 'rem'])
    @commands.guild_only()
    async def tag_delete(self, ctx, name):
        """Delete a tag"""
        archive = f'bot_data/tags/{ctx.guild.id}/{name}.json'
        if not os.path.isfile(archive): return await ctx.send('Tag not found')
        with open(archive, 'r') as f:
            data = json.load(f)
        if ctx.author.id != data['owner']: return await ctx.send('You did not make this tag.')
        os.remove(archive)
        await ctx.send('Tag removed')

    @cockatrice_tag.command(name='info')
    @commands.guild_only()
    async def tag_info(self, ctx, name):
        """Info on a tag"""
        archive = f'bot_data/tags/{ctx.guild.id}/{name}.json'
        if not os.path.isfile(archive): return await ctx.send('Tag not found')
        with open(archive, 'r') as f:
            data = json.load(f)
        owner = ctx.guild.get_member(data['owner'])
        if owner is None: owner = data['owner']
        else: owner = owner.name
        message = f'Tag **{name}**\n```lisp\n{data["code"]}\n```\nOwner: {owner}'
        await ctx.send(message)

    @functools.lru_cache(CACHE_SIZE)
    @cacheable
    async def get_location(self, location):
        return await self.nom.geocode(location)

    @commands.command(name='time')
    @commands.cooldown(1, 1, commands.BucketType.user)
    async def timezone_tool(self, ctx, *, location='UTC'):
        """Get local time in a location or timezone"""
        curr_time = dt.datetime.utcnow()
        namematches = list(filter(lambda x: dt.datetime.now(pytz.timezone(x)).strftime("%Z") == location.upper(),
                             pytz.common_timezones))

        if namematches:
            categories = {}
            texts = {}
            for tzname in namematches:
                tz = pytz.timezone(tzname)
                offset = tz.utcoffset(curr_time)
                if offset in categories:
                    categories[offset].append(f'*{tzname}*')
                    continue

                categories[offset] = [f'*{tzname}*']
                localtime = curr_time + tz.utcoffset(curr_time)
                twofour = localtime.strftime("%Y-%m-%d %H:%M:%S")
                twelve = localtime.strftime("%Y-%m-%d %I:%M:%S %p")
                texts[offset] = twofour + '\n' + twelve

            if len(categories) == 1:
                hdr = f'Time in **{location.upper()}**:\n'
                return await ctx.send(hdr+texts.popitem()[1])

            if len(categories) > 1:
                hdr = f'Ambiguous timezone **{location.upper()}**. Found the following:\n'
                for key in categories:
                    hdr += ', '.join(categories[key]) + '\n'
                    hdr += texts[key]+'\n\n'

                return await ctx.send(hdr)

        try:
            locname = location
            tz = pytz.timezone(location)
        except:
            location = await self.get_location(location)
            if location is None:
                return await ctx.send('Location could not be found')

            locname = location.address
            tzname = self.tf.timezone_at(lat=location.latitude, lng=location.longitude)
            if tzname is None:
                return await ctx.send('Could not find the timezone in that place. '
                                      'Please try a nearby location.')

            tz = pytz.timezone(tzname)

        curr_time += tz.utcoffset(curr_time)

        twofour = curr_time.strftime("%Y-%m-%d %H:%M:%S")
        twelve = curr_time.strftime("%Y-%m-%d %I:%M:%S %p")
        await ctx.send(f"The time in **{locname}** is\n{twofour}\n{twelve}")


def setup(bot):
    bot.add_cog(Utils(bot))


if __name__ == '__main__':
    cog = Utils(None)
