import random
import discord
import os
import itertools

import time
import shlex
import asyncio
from subprocess import Popen, PIPE

from statistics import mean, stdev
from ruamel import yaml
from discord.ext import commands
Cog = commands.Cog

RESP_DIR = 'cogs/responses'
PERCENTAGE = 0.85


def one_of_them(arg):
    if arg[0].lower() not in 'ab':
        raise ValueError()
    else:
        return arg[0].lower()


class Voting(Cog):

    def __init__(self, bot):
        self.bot = bot
        with open('votes.yml', 'r') as votes:
            self.votes = yaml.load(votes)

        self.can_vote = False
    '''
    async def __local_check(self, ctx):
        return self.bot.get_guild(386357756409675776).get_member(ctx.author.id) is not None
    '''
    def get_count(self, filename):
        acc = 0
        for vote in self.votes['votes'].values():
            for pair in vote:
                if filename in pair:
                    acc += 1

        for pair in self.votes['slides'].values():
            if pair != -1 and filename in pair:
                acc += 0.5

        return acc

    @commands.command()
    async def vote(self, ctx, response=None):
        """Use `vote` to get a fresh slide
        Vote on a slide with `vote a` or `vote b`
        """
        if not self.can_vote:
            return await ctx.send(
                'Voting is not enabled right now. '
                'Contact hanss314#0128 if you think this is a mistake'
            )
        if response is not None: response = response.lower()
        if response not in ['a', 'b', None]:
            return await ctx.send('I was expecting the vote to be `a` or `b`')

        if ctx.author.id in self.votes['slides'] and self.votes['slides'][ctx.author.id] == -1:
            tlow = self.bot.get_guild(386357756409675776)
            role = discord.utils.find(lambda r: r.id == 394197953033535489, tlow.roles)
            member = tlow.get_member(ctx.author.id)
            try:
                await member.add_roles(role)
            except (discord.Forbidden, AttributeError) as e:
                print(e)

            return await ctx.send('You\'ve voted on everything, please stop voting.')

        if not isinstance(ctx.channel, discord.abc.PrivateChannel):
            try:
                await ctx.message.delete()
            except discord.Forbidden:
                pass
            return await ctx.send('Please only vote in DMs.')

        create_new = False
        if ctx.author.id in self.votes['slides'] and response:
            # Success in most cases, so try except is faster
            try:
                votes = self.votes['votes'][ctx.author.id]
            except KeyError:
                votes = self.votes['votes'][ctx.author.id] = []

            slide = self.votes['slides'][ctx.author.id]
            votes.append((slide[0], slide[1]) if response == 'a' else (slide[1], slide[0]))
            create_new = True

        elif ctx.author.id not in self.votes['slides']:
            create_new = True

        if create_new:
            responses = []
            for path in os.listdir(RESP_DIR):
                file = os.path.join(RESP_DIR, path)
                if os.path.isfile(file):
                    responses.append((self.get_count(file), file))

            responses.sort()
            fewest = [x for x in responses if x[0] == responses[0][0]]
            rest = [x for x in responses if x not in fewest]
            random.shuffle(fewest)

            def voted_before(a, b):
                try:
                    for vote in self.votes['votes'][ctx.author.id]:
                        if a[1] in vote and b[1] in vote:
                            return True
                except KeyError:
                    self.votes['votes'][ctx.author.id] = []
                return False

            slide = None
            for x, y in itertools.product(fewest, fewest):
                if x == y: continue
                if x != y and not voted_before(x, y):
                    slide = (x[1], y[1])
                    break

            if slide is None:
                for x, y in itertools.product(fewest + rest, fewest + rest):
                    if x == y: continue
                    if x != y and not voted_before(x, y):
                        slide = (x[1], y[1])
                        break

            if slide is None:
                self.votes['slides'][ctx.author.id] = -1
                tlow = self.bot.get_guild(386357756409675776)
                role = discord.utils.find(lambda r: r.id == 394197953033535489, tlow.roles)
                member = tlow.get_member(ctx.author.id)
                try:
                    await member.add_roles(role)
                except (discord.Forbidden, AttributeError) as e:
                    print(e)
                return await ctx.send('You have voted on everything. Please stop voting.')

            self.votes['slides'][ctx.author.id] = slide

        def count_lines(lines):
            return len(lines.encode().rstrip().split(b'\n'))

        slide = self.votes['slides'][ctx.author.id]

        async def send_half(slide_part, code):
            try:
                content = open(slide_part, 'r').read()
            except UnicodeDecodeError:
                content = open(slide_part, 'rb').read()

            lang = slide_part.split(".")[-1]

            d = f'Pick the better one:\n:regional_indicator_{code}: '
            if slide_part in self.votes['broke']: d += '**Not working** '
            d += f'{count_lines(content)} line(s)'

            if len(content) < 1600:
                d += f'```{lang}\n{content}```\n'
                # d += f'Docs: {self.votes["docs"].get(slide_part.split("/")[-1].split(".")[0])}'
                return await ctx.send(d)

            d += f'```{lang}\n{content[:1600]}```\n'
            # d += f'Docs: {self.votes["docs"].get(slide_part.split("/")[-1].split(".")[0])}'
            await ctx.send(
                content=f'{d} *Too much content, see attached file*',
                file=discord.File(open(slide_part, 'rb'),
                                  filename=f'{code}.{lang}')
            )

        await send_half(slide[0], 'a')
        await send_half(slide[1], 'b')

    @commands.command()
    @commands.is_owner()
    async def results(self, ctx):
        def get_percentage(response):
            return 100 * sum(response['votes']) / response['count']

        def ordinal(num):
            return ['ˢᵗ', 'ᶮᵈ', 'ʳᵈ', 'ᵗʰ'][3 if num // 10 % 10 == 1 else min(num % 10 - 1, 3)]

        def get_power(vote_count):
            return 1 / vote_count

        responses = {
            os.path.join(RESP_DIR, path): [[], 0]
            for path in os.listdir(RESP_DIR) if os.path.isfile(os.path.join(RESP_DIR, path))
        }
        for votes in self.votes['votes'].values():
            if len(votes) == 0: continue
            choices = {r: [] for r in responses}
            for vote in votes:
                choices[vote[0]].append(1)
                choices[vote[1]].append(0)

            for r, v in choices.items():
                if len(v) == 0: continue
                responses[r][0].append(mean(v))
                responses[r][1] += len(v)
        print(responses)
        responses = [{
                'responder': r.split('/')[-1].split('.')[0],
                'response': r,
                'percentage': mean(p[0]) * 100,
                'stdev': stdev(p[0]),
                'votes': p[1]
            } for r, p in responses.items()
        ]

        responses.sort(key=lambda x: x['percentage'], reverse=True)
        for n, response in enumerate(responses):
            symbol = ordinal(n + 1)
            dead = n > len(responses) * PERCENTAGE
            try:
                content = open(response['response'], 'r').read()
            except UnicodeDecodeError:
                content = open(response['response'], 'rb').read()

            lang = response['response'].split(".")[-1]
            file = None
            if len(content) > 1800:
                content = '*Too much content, see attached file*'
                content += f'```{lang}\n{content[:1800]}```\n'
                file = discord.File(open(response['response'], 'r'), filename=f'response.{lang}')
            else:
                content = f'```{lang}\n{content}```\n'

            msg = f'{"=" * 50}\n' \
                  f'{":skull_crossbones:" if (dead and n != 0) else ":white_check_mark:"} ' \
                  f'**{n+1}{symbol} place**: {content}\n' \
                  f'**<@{response["response"].split(".")[0].split("/")[-1]}>** ' \
                  f'({response["percentage"]:.2f}%)'

            if file is not None:
                print(msg)
            await ctx.send(msg, file=file)
            with open('results.txt', 'a+') as results_file:
                results_file.write(msg + '\n\n\n')

    @commands.command()
    @commands.is_owner()
    async def toggle_voting(self, ctx):
        """Toggle voting"""
        self.can_vote = not self.can_vote
        if self.can_vote: await ctx.send('Voting enabled.')
        else: await ctx.send('Voting disabled.')

    @commands.command()
    @commands.is_owner()
    async def fully_voted(self, ctx):
        """See the people who have voted the max number of times"""
        await ctx.send(', '.join(
            self.bot.get_user(user).name for user in self.votes['slides'] if self.votes['slides'][user] == -1
        ) or 'Nobody')

    @commands.command()
    async def tlowctf(self, ctx, answer):
        """Attempt a TLOW CTF flag"""
        if ctx.guild is not None:
            await ctx.send("You must submit in DMs!")
            try: await ctx.message.delete()
            except: pass
            return

        #if self.bot.get_guild(297811083308171264).get_member(ctx.author.id) is None:
        if self.bot.get_guild(386357756409675776).get_member(ctx.author.id) is None:
            return await ctx.send('You must be on the TLOW server to do the CTF!')

        if f"{answer}.txt" not in os.listdir('_tlowctf'):
            return await ctx.send("Incorrect.")

        try: ctfhist = self.votes['ctf']
        except KeyError:
            ctfhist = self.votes['ctf'] = {}

        try:
            if ctx.author.id in ctfhist[answer]:
                return await ctx.send('You have already completed this flag!')
        except KeyError:
            ctfhist[answer] = []

        with open(f'_tlowctf/{answer}.txt', 'r') as ansfile:
            header = ansfile.readline()
            rest = ansfile.read()

        ctfhist[answer].append(ctx.author.id)

        await ctx.send(rest)

        await ctx.bot.get_channel(503025847238918145).send(
        #await ctx.bot.get_channel(368282610910363648).send(
            f'{ctx.author} has solved {header}'
        )

    @vote.after_invoke
    @tlowctf.after_invoke
    async def save(self, _):
        with open('votes.yml', 'w') as votes:
            yaml.dump(self.votes, votes)

    """
    @commands.command()
    @commands.is_owner()
    async def fight(self, ctx):
        moves = {
            0: 'Rock',
            1: 'Paper',
            2: 'Scissors',
        }
        competitors = {
            'python3 comp/136374601788686336.py': 'phenomist',
            'python3 comp/hanss.py': 'hanss314',
            'python3 comp/1615081656727633921.py': 'Bottersnike (1)',
            'python3 comp/161508165672763392.py': 'Bottersnike',
            'python3 comp/248156896131940354.py': '_zM',
            'comp/milo':'Milo Jacquet',
            'python3 comp/236257776421175296.py': '96 LB'
        }
        results = {competitor: 0 for competitor in competitors.values()}
        comps = list(competitors.keys())
        competitions = [
            (comps[x], comps[(x+i) % len(comps)])
            for i in range(1, len(comps))
            for x in range(len(comps) - i)
        ]

        async def fight(a, b):
            a_name, b_name = f"**{competitors[a]}**", f"**{competitors[b]}**"
            a = shlex.split(a)
            b = shlex.split(b)
            a_remaining = [17, 17, 17]
            b_remaining = [17, 17, 17]
            a_wins = 0
            b_wins = 0
            a_hist = ''
            b_hist = ''
            for i in range(51):
                a_proc = Popen(a + [b_hist, a_hist], stdout=PIPE, stderr=PIPE)
                b_proc = Popen(b + [a_hist, b_hist], stdout=PIPE, stderr=PIPE)
                a_out, a_err = a_proc.communicate()
                b_out, b_err = b_proc.communicate()
                a_fail = b_fail = False
                if a_err: a_fail = True
                elif b_err: b_fail = True

                try: a_out = int(a_out)
                except ValueError: a_fail = True
                try: b_out = int(b_out)
                except ValueError: b_fail = True

                if not a_fail and (a_out < 0 or a_out > 2): a_fail = True
                if not b_fail and (b_out < 0 or b_out > 2): b_fail = True

                if not a_fail:
                    a_remaining[a_out] -= 1
                    a_hist += str(a_out)
                    if a_remaining[a_out] < 0: a_fail = True
                else:
                    a_hist += '3'

                if not b_fail:
                    b_remaining[b_out] -= 1
                    b_hist += str(b_out)
                    if b_remaining[b_out] < 0: b_fail = True
                else:
                    b_hist += '3'

                if a_fail and b_fail:
                    await ctx.send('Both contestants forfeit! This round is a draw!')
                elif a_fail:
                    await ctx.send(f'{a_name} forfeits!')
                    b_wins += 1
                elif b_fail:
                    await ctx.send(f'{b_name} forfeits!')
                    a_wins += 1
                else:
                    playline = f'{a_name} plays `{moves[a_out]}` {b_name} plays `{moves[b_out]}` '
                    if a_out == b_out:
                        await ctx.send(playline + "It's a tie!")
                    else:
                        if (a_out - b_out) % 3 == 1:
                            winner = a_name
                            a_wins += 1
                        else:
                            winner = b_name
                            b_wins += 1

                        await ctx.send(playline + f'{winner} wins!')

                await asyncio.sleep(10)

            return a_wins, b_wins

        await ctx.send('The competition starts now!')
        start_times = [time.time() + x * 6 * 60 * 60 / len(competitions) for x in range(1, len(competitions) + 1)]
        start_times[-1] = 0
        for pairing, timing in zip(competitions, start_times):
            await ctx.send(f'**{competitors[pairing[0]]}** vs. **{competitors[pairing[1]]}**')
            wins = await fight(*pairing)
            await ctx.send(
                f'{competitors[pairing[0]]} wins {wins[0]} time(s). '
                f'{competitors[pairing[1]]} wins {wins[1]} time(s).'
            )
            results[competitors[pairing[0]]] += wins[0]
            results[competitors[pairing[1]]] += wins[1]
            yaml.dump(results, open('bot_data/comp_results.yml', 'w'))
            if timing == 0:
                results = list(results.items())
                results.sort(key=lambda p: p[1])
                results.reverse()
                await ctx.send(
                    f"**Final results:** \n{chr(10).join('*{}*: {}'.format(*entry) for entry in results)}"
                )
                yaml.dump(results, open('bot_data/comp_results.json', 'w'))
                break

            elif time.time() < timing:
                wait_time = timing - time.time()
                await ctx.send(f'The next match starts in {int(wait_time//60):02d}:{int(wait_time%60):02d}')
                await asyncio.sleep(wait_time)
            else:
                await ctx.send('The next match starts now!')
    """

def setup(bot):
    bot.add_cog(Voting(bot))
