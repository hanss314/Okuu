import discord
import random

from .UTTT.quarto import QuartoGame, quartopiece
from .UTTT.board import UTTT_Board
from discord.ext import commands
from ruamel import yaml
from .util.data import get_data, save_data
#from .util.coupgame import rungame
Cog = commands.Cog

def avocados(arg):
    try: return int(arg)
    except ValueError:
        return len([c for c in arg if ord(c) == 129361])


class Games(Cog):

    def __init__(self, bot):
        self.bot = bot
        self.coupchannels = set()
        try:
            f = open('bot_data/quarto.yml', 'r')
        except FileNotFoundError:
            open('bot_data/quarto.yml', 'w').write('{}')
            self.games = {}
        else:
            self.games = yaml.safe_load(f)
            f.close()

    @staticmethod
    def get_game(player, db, emptytype, auto_join=True, constr_args=()):
        game = None
        for k in db.keys():
            if player in k:
                game = k
                break
            if len(k) == 1:
                game = k

        if not game:
            if not auto_join: raise ValueError
            board = emptytype(*constr_args)
            game = (player,)
            db[game] = board
        elif len(game) == 1 and player not in game:
            if not auto_join: raise ValueError
            board = db[game]
            del db[game]
            game = (game[0], player)
            db[game] = board
        else:
            board = db[game]

        return game, board

    @commands.group(invoke_without_command=True)
    @commands.guild_only()
    async def uttt(self, ctx, row: int, column: int):
        '''
        Use this command to start and play a uttt game.
        To start a game, the x and y coordinates are the sub-board of your first move
        Use this command to specify the sub-board when needed and make a move when sub-board is specified
        '''
        x, y = row, column
        boards = self.bot.uttt_boards[ctx.guild.id]
        board, game = self.get_game(ctx.author.id, boards, UTTT_Board)

        player = game.index(ctx.author.id) + 1
        if board.turn != player:
            return await ctx.send(f'{ctx.author.mention} It\'s not your turn!')

        if board.next_play == 0:
            legal_moves = [x[:2] for x in board.get_legal_moves()]
            if (x, y) in legal_moves:
                board.next_play = (x, y)
                await ctx.send(
                    f'{ctx.author.mention}, your move will be made on board {board.next_play} ```\n{board.to_UI()}```'
                )
                return
            else:
                await ctx.send(f'{ctx.author.mention} that is not a playable board.')
                return

        else:
            move = (*board.next_play, (x, y))
            if move not in board.get_legal_moves():
                return await ctx.send(f'{ctx.author.mention} that is not a legal move.')
            winner = board.play_move(move, board.turn)
            board.turn = board.turn % 2 + 1
            if winner != 0:
                await ctx.send(f'<@{game[winner-1]}> has won! ```\n{board.to_UI()}```')
                del boards[game]
                del board
                return
            else:
                if len(game) == 1:
                    d = 'Anyone\'s turn'
                else:
                    d = f'<@{game[board.turn-1]}> your turn'

                if board.next_play == 0:
                    d += f', select a sub-board to play on using `{ctx.prefix}utt play` ```\n{board.to_UI()}```'
                else:
                    d += f', you are playing on sub-board {board.next_play}. ```\n{board.to_UI()}```'

                await ctx.send(d)

    @uttt.before_invoke
    async def check_board(self, ctx):
        if not hasattr(self.bot, 'uttt_boards'):
            self.bot.uttt_boards = {}
        if ctx.guild.id not in self.bot.uttt_boards:
            self.bot.uttt_boards[ctx.guild.id] = {}

    @uttt.command(name='cancel')
    @commands.guild_only()
    async def uttt_cancel(self, ctx):
        '''Cancel a game of UTTT'''
        boards = self.bot.uttt_boards[ctx.guild.id]
        for k in boards.keys():
            if ctx.author.id in k:
                game = k
                break
        else:
            return await ctx.send('You have no game running.')

        del boards[game]
        if len(game) > 1:
            other = ctx.guild.get_member(game[game.index(ctx.author.id)-1])
            await ctx.send(f'{ctx.author.mention} Cancelled your game against {other.mention}.')
        else:
            await ctx.send(f'{ctx.author.mention} Game cancelled.')

    @uttt.command(name='help')
    async def uttt_help(self, ctx):
        '''Gives you help on UTTT'''
        d = '**Rules:**\n'
        d += '''
Win three games of Tic Tac Toe in a row. 
You may only play in the big field that corresponds to the last small field your opponent played. 
When your are sent to a field that is already decided, you can choose freely. 
<https://mathwithbaddrawings.com/2013/06/16/ultimate-tic-tac-toe/>
        '''
        await ctx.send(d)


    @commands.command()
    async def mines(self, ctx, width: int=9, height: int=9, bombs: int=9, style='n'):
        """Play minesweeper with spoiler tags.
        I will attempt to make corners safe
        Style can be `n` for Normal or `k` for Knight.
        """
        names = ['bomb', 'white_large_square', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight']
        style = style[0:].lower()
        def normhood(x, y):
            return {
                (x+i, y+j) for i in range(-1, 2) for j in range(-1, 2)
                if 0 <= x+i < width and 0 <= y+j < height and (i, j) != (0, 0)
            }

        def knighthood(x, y):
            vals = (-2, -1, 1, 2)
            return {
                (x+i, y+j) for i in vals for j in vals
                if 0 <= x+i < width and 0 <= y+j < height and abs(i) != abs(j)
            }

        hoods = {'n': normhood, 'k': knighthood}

        if width < 1 or height < 1 or bombs < 0:
            return await ctx.send("I can't give you something that doesn't exist!")

        if style not in ('n','k'):
            return await ctx.send("Style must be `k` or `n`!")

        board = [[0 for _ in range(width)] for __ in range(height)]
        tiles = [(x, y) for x in range(width) for y in range(height)]
        random.shuffle(tiles)
        mines = tiles[:bombs]
        for mine in mines:
            board[mine[1]][mine[0]] = -1

        for x in range(width):
            for y in range(height):
                if board[y][x] != 0: continue
                mines = len([0 for x, y in hoods[style](x, y) if board[y][x] == -1])
                board[y][x] = mines

        to_check = []
        reveal = set()
        for x in range(width):
            for y in range(height):
                if board[y][x] == 0:
                    to_check.append((x, y))
                    reveal.add((x, y))
                    break
            else:
                continue
            break

        while to_check:
            c = to_check.pop()
            neighborhood = hoods[style](*c)
            for x, y in neighborhood:
                if (x,y) not in reveal and board[y][x] == 0:
                    to_check.append((x, y))

                reveal.add((x, y))

        name = 'Minesweeper' if style == 'n' else 'Knightsweeper'
        s = f'{name}: {width}x{height}. {bombs}:bomb:\n'
        for y, row in enumerate(board):
            for x, tile in enumerate(row):
                if (x, y) in reveal:
                    s += f':{names[tile+1]}:'
                else:
                    s += f'||:{names[tile+1]}:||'

            s += '\n'

        return await ctx.send(s)


    @commands.group(invoke_without_command=True)
    async def quarto(self, ctx):
        """The quarto game"""
        await ctx.send("Instructions later. Ask Azurite")

    @quarto.command(name='new')
    @commands.guild_only()
    async def quarto_new(self, ctx, opponent: discord.Member = None):
        """Create a new game"""
        pid = (ctx.guild.id, ctx.author.id)
        if pid in self.games:
            return await ctx.send('Unyu? You are already part of a game')

        game = QuartoGame(ctx.guild.id, ctx.author.id)
        self.games[pid] = game
        if opponent is not None:
            nid = (ctx.guild.id, opponent.id)
            game.add_player(opponent.id)
            self.games[nid] = game
            return await ctx.send(f'New game created with {opponent.mention} ```{game}```')
        else:
            return await ctx.send(f'New game created. ```{game}```')

    @quarto.command(name='join')
    @commands.guild_only()
    async def quarto_join(self, ctx, player: discord.Member):
        """Join a game by a player"""
        pid = (ctx.guild.id, player.id)
        nid = (ctx.guild.id, ctx.author.id)
        if pid not in self.games:
            return await ctx.send('Unyu? That game doesn\'t exist.')

        game = self.games[pid]
        if len(game.players) > 1:
            return await ctx.send('You can\'t join this game')

        game.add_player(ctx.author.id)
        self.games[nid] = game
        await ctx.send('Game joined!')

    @quarto.command(name='board')
    @commands.guild_only()
    async def quarto_board(self, ctx):
        """Show your board"""
        pid = (ctx.guild.id, ctx.author.id)
        if pid not in self.games:
            return await ctx.send('Unyu? That game doesn\'t exist.')

        await ctx.send(f'```{self.games[pid]}```')

    @quarto.command(name='delete')
    @commands.guild_only()
    async def quarto_delete(self, ctx):
        """Delete a game you are in"""
        pid = (ctx.guild.id, ctx.author.id)
        if pid not in self.games:
            return await ctx.send('Unyu? You aren\'t in a game here')

        game = self.games[pid]
        pid = (ctx.guild.id, game.players[0])
        nid = (ctx.guild.id, game.players[1])
        del self.games[pid]
        del self.games[nid]
        return await ctx.send('Game deleted')

    @quarto.command(name='move')
    @commands.guild_only()
    async def quarto_move(self, ctx, x: int, y: int, piece: quartopiece):
        """Place a piece. Piece notation
        `cq` Circle or sQuare
        `sl` Small or Large
        `hf` Hollow or Filled
        `rb` Red or Blue
        """
        pid = (ctx.guild.id, ctx.author.id)
        if pid not in self.games:
            return await ctx.send('Unyu? You aren\'t in a game here')

        game = self.games[pid]
        if not game.is_turn(ctx.author.id):
            return await ctx.send('It\'s not your turn!')

        stat, info = game.make_move(x, y, piece)
        if stat == -1:
            response = f'You can\'t do that becase {info}'
        elif stat == 0:
            response = f'<@{game.players[game.turn]}>\'s turn \n```{game}```'
        else:
            response = f'<@{game.players[info]}> wins! \n```{game}```'
            del self.games[game.g, game.players[0]]
            del self.games[game.g, game.players[1]]

        await ctx.send(response)
    """
    @commands.command()
    async def coupgame(self, ctx):
        if ctx.channel.id in self.coupchannels:
            return await ctx.send('There is already a game happening!')
        self.coupchannels.add(ctx.channel.id)
        await rungame(self.bot, ctx)
        self.coupchannels.discard(ctx.channel.id)
    """

def setup(bot):
    bot.add_cog(Games(bot))
