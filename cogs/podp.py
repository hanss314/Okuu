'''
import discord
import time

from datetime import datetime, timedelta
from .util.data import save_data, get_data
from discord.ext import commands

DATA_FILE = 'podp.yml'
DEBUG = False
ARCHIVE_LIMIT = 15

if DEBUG:
    GUILD = 297811083308171264
    CHANS = 544948239154282497
    DISCUSS = 544961886240899073
    ARCHIVE = 544948370947702785
else:
    GUILD = 528078449676648470
    CHANS = 528079576811700234
    DISCUSS = 544053712125952031
    ARCHIVE = 536807014123175937

OW_NR = discord.PermissionOverwrite(
    read_messages=False,
    send_messages=False,
    read_message_history=False
)
OW_RO = discord.PermissionOverwrite(
    read_messages=True,
    read_message_history=True,
    send_messages=False
)
OW_RWE = discord.PermissionOverwrite(
    manage_channels=True,
    manage_roles=True,
    manage_webhooks=True,
    read_messages=True,
    send_messages=True,
    send_tts_messages=False,
    manage_messages=True,
    embed_links=True,
    attach_files=True,
    read_message_history=True,
    mention_everyone=False,
    external_emojis=True,
    add_reactions=True,
)
OW_RW = discord.PermissionOverwrite(
    read_messages=True,
    send_messages=True,
    read_message_history=True
)

OW_RE = discord.PermissionOverwrite(
    read_messages=True,
    manage_roles=True
)

def default():
    return {'users': {}, 'discussions': {}}

class PoDP:
    def __init__(self, bot):
        self.bot = bot
        self.channels = get_data(DATA_FILE, default)
        self.privates = self.channels['discussions']
        self.channels = self.channels['users']
        self.discuss = self.guild = self.chancat = self.archive = None
        self.last_check = time.time()
        bot.loop.create_task(self.on_ready())
        bot.load_extension('cogs._podp')

    @staticmethod
    async def __local_check(ctx):
        return ctx.guild is not None and ctx.guild.id == GUILD

    async def local_check(self, ctx):
        return await self.__local_check(ctx)

    async def on_ready(self):
        self.guild = self.bot.get_guild(GUILD)
        self.chancat = self.guild.get_channel(CHANS)
        self.discuss = self.guild.get_channel(DISCUSS)
        self.archive = self.guild.get_channel(ARCHIVE)
        await self.sync_perms()


    async def sync_perms(self):
        for c, d in self.privates.items():
            channel = self.guild.get_channel(c)
            discussion = self.guild.get_channel(d)
            for ow in channel.overwrites:
                try:
                    if isinstance(ow[0], discord.Member) and ow[1].read_messages:
                        await discussion.set_permissions(ow[0], overwrite=OW_RW)
                    elif isinstance(ow[0], discord.Member) and not ow[1].read_messages:
                        await discussion.set_permissions(ow[0], overwrite=None)
                except discord.Forbidden:
                    break


    async def check_archival(self):
        for channel in self.channels.values():
            channel = self.guild.get_channel(channel)
            async for message in channel.history(limit=1):
                if message.created_at + timedelta(days=ARCHIVE_LIMIT) < datetime.today():
                    await self.insert_channel(channel, self.archive)

                break

    async def on_message(self, message):
        if message.guild is None or message.guild.id != GUILD: return
        if message.channel.category is not None and \
                message.channel.category.id == self.archive.id:
            await self.insert_channel(message.channel, self.chancat)

        if time.time() > self.last_check + 60*60*24:
            self.last_check = time.time()
            await self.check_archival()

    async def on_guild_channel_update(self, before, after):
        if before.guild.id != self.guild.id: return
        if not before.id in self.privates: return
        channel = self.guild.get_channel(self.privates[before.id])
        to_add = [ow for ow in after.overwrites if ow not in before.overwrites]
        to_remove = [ow for ow in before.overwrites if ow not in after.overwrites]
        owner = [k for k, v in self.channels.items() if v == before.id][0]
        for ow in to_remove:
            if ow[0].id == owner: continue
            if isinstance(ow[0], discord.Member):
                await channel.set_permissions(ow[0], overwrite=None)
        print(to_add)
        for ow in to_add:
            if ow[0].id == owner: continue
            if isinstance(ow[0], discord.Member) and ow[1].read_messages:
                await channel.set_permissions(ow[0], overwrite=OW_RW)
            elif isinstance(ow[0], discord.Member) and not ow[1].read_messages:
                await channel.set_permissions(ow[0], overwrite=None)

    @staticmethod
    def bin_search(start, end, channels, name):
        if end <= start: return start

        piv = ((end-start) // 2) + start
        if piv == start: return start
        elif piv == end: return end
        if channels[piv].name.lower() > name:
            return PoDP.bin_search(start, piv, channels, name)
        else:
            return PoDP.bin_search(piv, end, channels, name)
        
    async def insert_channel(self, channel, category):
        channels = self.guild.by_category()
        channels = [c[1] for c in channels if c[0] is not None and c[0].id == category.id]
        if channels: channels = channels[0]
        i = channels[0].position if channels else 1
        position = self.bin_search(0, len(channels), channels, channel.name.lower()) + i
        await channel.edit(
            category=category,
            position=position
        )

    @commands.command(aliases=['newchan'])
    async def new_channel(self, ctx):
        """Create a new channel for yourself"""
        if ctx.author.id in self.channels:
            return await ctx.send(
                f'You already have your channel in <#{self.channels[ctx.author.id]}>!'
            )

        channel = await self.guild.create_text_channel(
            ctx.author.name.lower(),
            overwrites={self.guild.default_role: OW_RO, ctx.author: OW_RWE},
        )
        await self.insert_channel(channel, self.chancat)
        self.channels[ctx.author.id] = channel.id
        await ctx.send(f'{ctx.author.mention} Created {channel.mention}')

    @commands.command(aliases=['credis'])
    async def create_discussion(self, ctx):
        """Create a discussion channel for your channel
        **Please don't use if you have a public channel**
        """
        if ctx.author.id not in self.channels:
            return await ctx.send("You don't have a channel!")
        channel = self.guild.get_channel(self.channels[ctx.author.id])
        overwrites = channel.overwrites_for(self.guild.default_role)
        if overwrites.read_messages:
            return await ctx.send(
                'You must have a private channel to create a discussion channel.'
            )
        new_channel = await self.guild.create_text_channel(
            ctx.author.name.lower(),
            overwrites={self.guild.default_role: OW_NR, ctx.author: OW_RWE},
        )
        await self.insert_channel(new_channel, self.discuss)
        await ctx.send(f'{ctx.author.mention} Created {new_channel.mention}')
        self.privates[channel.id] = new_channel.id

    @commands.command()
    @commands.has_permissions(manage_roles=True)
    @commands.bot_has_permissions(manage_roles=True)
    async def unwhitelist(self, ctx, blocked: commands.Greedy[discord.Member], writable: bool = False):
        """Whitelist everyone except for a particular user

        The channel will be blocked for everyone.
        All users not blocked will be permitted into the channel.
        To allow permitted users to also send messages, append "yes" as the final argument.
        Please give the bot manage permissions in your channel as well as read messages beforehand.
        """
        blocked = set(map(lambda x:x.id, blocked))
        if writable: overwrite = OW_RW
        else: overwrite = OW_RO
        await ctx.channel.set_permissions(ctx.guild.me, overwrite=OW_RE)
        await ctx.channel.set_permissions(ctx.author, overwrite=OW_RWE)
        await ctx.channel.set_permissions(ctx.guild.default_role, overwrite=OW_NR)
        for member in ctx.guild.members:
            if member.id == ctx.author.id or member.id == self.bot.user.id: continue
            if member.id in blocked:
                await ctx.channel.set_permissions(member, overwrite=OW_NR)
            else:
                await ctx.channel.set_permissions(member, overwrite=overwrite)

    @commands.command()
    @commands.has_permissions(manage_roles=True)
    @commands.bot_has_permissions(manage_roles=True)
    async def clear_perms(self, ctx):
        """Clear user specific permissions from the channel.

        Will not clear for utsuho or the person who ran the command
        """
        for member in ctx.guild.members:
            if member.id == ctx.author.id or member.id == self.bot.user.id: continue
            await ctx.channel.set_permissions(member, overwrite=None)


    @new_channel.after_invoke
    @create_discussion.after_invoke
    async def save(self, _):
        save_data(DATA_FILE)



def setup(bot):
    bot.add_cog(PoDP(bot))
'''