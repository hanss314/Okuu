import discord
import datetime
import time
import urllib.parse
import os
import random
import aiohttp

from typing import Dict, Any

from .util.google import get_google_xkcd
# from .util import touhouwiki

from discord.ext import commands
Cog = commands.Cog

IMAGE_PREFIX = "images/images/"
XKCD_ENDPOINT = 'https://xkcd.com/{}/info.0.json'


class Misc(Cog):
    def __init__(self, bot):
        # import importlib
        self.bot = bot
        # importlib.reload(touhouwiki)
        try: bot.load_extension('cogs._trivial')
        except: pass
        # self.spellcards = touhouwiki.get_spellcards()

    @staticmethod
    def format_args(cmd):
        params = list(cmd.clean_params.items())
        p_str = ''
        for p in params:
            if p[1].default == p[1].empty:
                p_str += f' <{p[0]}>'
            else:
                p_str += f' [{p[0]}]'

        return p_str

    def format_commands(self, prefix, cmd, name=None):
        cmd_args = self.format_args(cmd)
        if not name: name = cmd.name
        name = name.replace('  ', ' ')
        d = f'`{prefix}{name}{cmd_args}`\n'

        if type(cmd) == commands.core.Group:
            cmds = sorted(list(cmd.commands), key=lambda x: x.name)
            for subcmd in cmds:
                d += self.format_commands(prefix, subcmd, name=f'{name} {subcmd.name}')

        return d

    def get_help(self, ctx, cmd, name=None):
        d = f'Help for command `{cmd.name}`:\n'
        d += '\n**Usage:**\n'
        if cmd.cog_name == 'FourBot': pfx = '4.'
        else: pfx = ctx.prefix
        d += self.format_commands(pfx, cmd, name=name)

        d += '\n**Description:**\n'
        d += '{}\n'.format('None' if cmd.help is None else cmd.help.strip())

        if cmd.aliases:
            d += '\n**Aliases:**'
            for alias in cmd.aliases:
                d += f'\n`{pfx}{alias}`'

            d += '\n'

        return d

    @staticmethod
    async def fetch_comic_data(number=None, *, latest=False):
        async with aiohttp.ClientSession() as session:
            async with session.get(XKCD_ENDPOINT.format(number if not latest else '')) as resp:
                return await resp.json()

    @staticmethod
    async def post_comic(ctx: commands.Context, metadata: Dict[str, Any]):
        embed = discord.Embed(title='#{}'.format(metadata['num']), description=metadata['safe_title'],
                              url='https://xkcd.com/{}/'.format(metadata['num']), colour=0xFFFFFF)
        embed.set_image(url=metadata['img'])
        embed.set_footer(text=metadata['alt'])

        return await ctx.send(embed=embed)

    @commands.command()
    @commands.guild_only()
    async def me(self, ctx, member: discord.Member = None):
        """Get info about yourself."""
        if not member: member = ctx.author
        now = datetime.datetime.utcnow()
        joined_days = now - member.joined_at
        created_days = now - member.created_at
        avatar = member.avatar_url

        embed = discord.Embed(colour=member.colour)
        embed.add_field(name='Nickname', value=member.display_name)
        embed.add_field(name='User ID', value=member.id)
        embed.add_field(name='Avatar', value='[Click here to show]({})'.format(avatar))

        embed.add_field(name='Created',
                        value=member.created_at.strftime('%x %X') + '\n{} days ago'.format(max(0, created_days.days)))
        embed.add_field(name='Joined',
                        value=member.joined_at.strftime('%x %X') + '\n{} days ago'.format(max(0, joined_days.days)))
        roles = '\n'.join(
            [r.mention for r in sorted(member.roles, key=lambda x: x.position, reverse=True) if r.name != '@everyone'])
        if roles == '': roles = '\\@everyone'
        embed.add_field(name='Roles', value=roles)

        embed.set_author(name=member, icon_url=avatar)

        try:
            await ctx.channel.send(embed=embed)
        except discord.Forbidden:
            pass

    @commands.command()
    async def invite(self, ctx):
        """Invite me to your server!"""
        await ctx.send(
            '*Invite me to your server:* ' +
            f'<https://discordapp.com/oauth2/authorize?client_id={ctx.bot.user.id}&scope=bot>'
        )

    @commands.command(aliases=['ping'])
    async def latency(self, ctx):
        """View websocket and message send latency."""

        rtt_before = time.monotonic()
        message = await ctx.send('Ping...')
        rtt_after = time.monotonic()
        rtt_latency = round((rtt_after - rtt_before) * 1000)

        await message.edit(content=f'WS: **{ctx.bot.latency*1000:.0f} ms**\nRTT: **{rtt_latency} ms**')

    @commands.command(aliases=['g'])
    async def google(self, ctx, *, query: str):
        """Google for a query"""
        op = urllib.parse.urlencode({'q': query})
        await ctx.send(f'https://google.com/search?{op}&safe=active')

    @commands.command(aliases=['v'])
    async def version(self, ctx, *, query=None):
        """Get a package version from the Arch repositories or the AUR"""
        if query is None:
            return await ctx.send('Unyu? This is a rolling release. No versions here.')

        aur = False
        async with aiohttp.ClientSession() as session:
            async with session.get(f'https://www.archlinux.org/packages/search/json/?name={query}') as resp:
                response = await resp.json()

            if not response['results']:
                aur = True
                async with session.get(f'https://aur.archlinux.org/rpc/?v=5&type=search&by=name&arg={query}') as resp:
                    response = await resp.json()

                response['results'] = [r for r in response['results'] if r['Name'] == query]
                if not response['results']: return await ctx.send(f'Unyu? There\'s no package called {query}')
                version = response['results'][0]['Version']
            else:
                version = response['results'][0]['pkgver']

        await ctx.send(f'`{query}` on the {"AUR" if aur else "Arch Repository"} is at version `{version}`')

    @commands.command()
    async def about(self, ctx):
        """About the bot"""
        d = 'This bot was made by *hanss314#0128*\n' \
            'Source code can be found at <https://github.com/hanss314/Okuu>\n' \
            f'Use {ctx.prefix}help for help'

        await ctx.send(d)

    @commands.command()
    async def dab(self, ctx):
        """Dab at the haters"""
        await ctx.send('<:nueDab:404506021114150922>')

    @commands.group(name='image', aliases=['i', 'img'], invoke_without_command=True)
    async def get_image(self, ctx, *image):
        """Get an image"""
        directory = IMAGE_PREFIX
        if any(map(
            lambda s: any(map(lambda c: c in s, '~./\\')), image
        )):
            return await ctx.send('No')

        dirpriority = False
        if image and image[-1] == '--random':
            dirpriority = True
            image = image[:-1]

        directory = os.path.join(directory, *image)
        isdir = False
        if os.path.isdir(directory) or os.path.islink(directory): isdir = True

        if not (isdir and dirpriority):
            for ext in ['png', 'jpg']:
                if os.path.isfile(f'{directory}.{ext}'):
                    isdir = False
                    directory = f'{directory}.{ext}'
                    break

            else:
                await ctx.send('Unyu? I can\'t find that image.')

        if isdir:
            while os.path.isdir(directory) or os.path.islink(directory):
                f = os.listdir(directory)
                if not f: return await ctx.send('Unyu? I can\'t find that image.')
                f = random.choice(f)
                directory = os.path.join(directory, f)

            await ctx.send(file=discord.File(directory))

        else:
            await ctx.send(file=discord.File(directory))


    @get_image.command(name='list')
    async def image_list(self, ctx, *path):
        """List all images"""
        directory = IMAGE_PREFIX
        if any(map(
                lambda s: any(map(lambda c: c in s, '~./\\')), path
        )):
            return await ctx.send('No')

        directory = os.path.join(directory, *path)

        if not os.path.isdir(directory) or os.path.islink(directory):
            return await ctx.send('Image group not found')


        images = list(set(os.listdir(directory)))
        images.sort()
        isdir = lambda x: os.path.isdir(x) or os.path.islink(x)
        images = [
            f'**{s}**' if isdir(os.path.join(directory, s)) else s.split('.')[0]
            for s in images
        ]
        images = ', '.join(images)
        await ctx.send(f'List of available images\n\n{images}')


    @commands.command()
    async def asakura(self, ctx):
        """A Sakura"""
        await ctx.send(file=discord.File('images/images/sakura.png'))

    @commands.command()
    async def xkcd(self, ctx, *, comic=None):
        """Search for xkcd comics"""
        latest = await self.fetch_comic_data(latest=True)
        if comic is None or comic == 'latest':
            return await self.post_comic(ctx, latest)

        elif comic == 'random':
            comic_number = random.randint(1, latest['num'])
            target = await self.fetch_comic_data(comic_number)

            return await self.post_comic(ctx, target)

        try: comic = int(comic)
        except ValueError: is_int = False
        else: is_int = True

        if is_int:
            if comic > latest['num']:
                return await ctx.send(f"Unyu? There's only {latest['num']} comics available...")
            elif comic < 1:
                return await ctx.send(f"Unyu? That one doesn't exist.")
            elif comic == 404:
                return await ctx.send(f"Unyu? I couldn't find it.")

            target = await self.fetch_comic_data(comic)
            return await self.post_comic(ctx, target)

        else:
            with ctx.typing():
                try: searches = await get_google_xkcd(comic)
                except RuntimeError:
                    return await ctx.send('Unyu? The search didn\'t work.')

                if len(searches) == 0:
                    return await ctx.send('Unyu? I didn\'t find anything.')

                comic = int(searches[0])
                target = await self.fetch_comic_data(comic)
                return await self.post_comic(ctx, target)

    @commands.command()
    @commands.has_permissions(ban_members=True)
    async def hackban(self, ctx, user_id: int, *, reason=''):
        """Ban a user by ID"""
        user = discord.Object(user_id)
        try:
            await ctx.guild.ban(user, reason=reason)
            await ctx.send(f'Banned <@{user_id}>')
        except discord.Forbidden:
            await ctx.send('I do not have permissions.')
        except discord.HTTPException:
            await ctx.send('Banning failed, did you type the id correctly?')

    @commands.command(aliases=['tatsukete_eirin', 'eirin', 'tatsukete'])
    async def help(self, ctx, *args):
        """This help message"""
        async def maybe_await(f):
            import inspect
            if inspect.isawaitable(f): return await f
            return f

        if len(args) == 0:
            cats = [name for name, cog in self.bot.cogs.items()
                    if not hasattr(cog, 'cog_check') or (await maybe_await(cog.cog_check(ctx)))]

            cats.sort()
            width = max([len(cat) for cat in cats]) + 2
            d = '**Categories:**\n'
            for cat in zip(cats[0::2], cats[1::2]):
                d += '**`{}`**{}**`{}`**\n'.format(cat[0], ' ' * int(2.3 * (width-len(cat[0]))), cat[1])
            if len(cats) % 2 == 1:
                d += '**`{}`**\n'.format(cats[-1])

            d += f'\nUse `{ctx.prefix}help <category>` to list commands in a category.\n'
            d += f'Use `{ctx.prefix}help <command>` to get in depth help for a command.\n'

        elif len(args) == 1:
            cats = {cog.lower(): cog for cog in self.bot.cogs}
            if args[0].lower() in cats:
                cog_name = cats[args[0].lower()]
                d = 'Commands in category **`{}`**:\n'.format(cog_name)
                cmds = self.bot.cogs[cog_name].get_commands()
                if cog_name == 'FourBot': pfx = '4.'
                else: pfx = ctx.prefix
                for cmd in sorted(list(cmds), key=lambda x: x.name):
                    try:
                        if cmd.hidden or not (await cmd.can_run(ctx)): continue
                    except commands.CheckFailure:
                        pass
                    d += '\n  `{}{}`'.format(pfx, cmd.name)

                    brief = cmd.brief
                    if brief is None and cmd.help is not None:
                        brief = cmd.help.split('\n')[0]

                    if brief is not None:
                        d += ' - {}'.format(brief)
                d += '\n'
            else:
                if args[0] not in ctx.bot.all_commands:
                    d = 'Unyu?'
                else:
                    cmd = ctx.bot.all_commands[args[0]]
                    d = self.get_help(ctx, cmd)
        else:
            d = ''
            cmd = ctx.bot
            cmd_name = ''
            for i in args:
                i = i.replace('@', '@\u200b')
                if cmd == ctx.bot and i in cmd.all_commands:
                    cmd = cmd.all_commands[i]
                    cmd_name += cmd.name + ' '
                elif type(cmd) == commands.Group and i in cmd.all_commands:
                    cmd = cmd.all_commands[i]
                    cmd_name += cmd.name + ' '
                else:
                    d += 'Unyu?'
                    break

            else:
                d = self.get_help(ctx, cmd, name=cmd_name)

        # d += '\n*Made by hanss314#0128*'
        return await ctx.send(d)

    @commands.command(aliases=['alive'])
    @commands.guild_only()
    async def botsonline(self, ctx, member: discord.Member = None):
        """Check which bots are online"""
        if member is not None:
            if member.status == discord.Status.offline: await ctx.send(f"{member} is offline")
            else: await ctx.send(f"{member} is online")
            return

        members = list(filter(lambda b: b.bot, ctx.guild.members))
        members.sort(key=lambda x: (x.name.lower(), x.discriminator))
        def statusstr(s):
            if s == discord.Status.online: return 'Alive <:online:328659633147215884>'
            elif s == discord.Status.offline: return 'Dead <:offline:328659633214324757>'
            elif s == discord.Status.idle: return 'Idle <:idle:328670650220806144>'
            elif s == discord.Status.dnd: return 'Do Not Disturb <:dnd:328659633109598208>'
            else: return str(s)

        await ctx.send('\n'.join(map(lambda b: f'**{b}**: {statusstr(b.status)}', members)))


def setup(bot):
    bot.add_cog(Misc(bot))
