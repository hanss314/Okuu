import discord
import random

from discord.ext import commands
from ruamel import yaml

from cogs.util.touhouwiki import touhouwiki

@commands.group(invoke_without_command=True)
async def mafia(self, ctx):
    '''Play a game of mafia'''
    await ctx.send(f'Use `{ctx.prefix}mafia join` to join a game of mafia on this server!')

@mafia.command(name='join')
async def mafia_join(self, ctx):
    '''
    Join a game of mafia in this channel
    Use `mafia start` to vote to start the game
    '''
    if ctx.channel.id in self.mafia_games:
        game = self.mafia_games[ctx.channel.id]
    else:
        game = self.mafia_games[ctx.channel.id] = mafia.new_game(ctx.channel.id)

    if ctx.author.id in game['players']:
        return await ctx.send(
            f'You have already joined! Use `{ctx.prefix}mafia start` to vote to start the game.'
        )

    for existing_game in self.mafia_games.values():
        if ctx.author.id in existing_game['players']:
            return await ctx.send('You are in another game!')

    game['players'][ctx.author.id] = mafia.new_player()
    await ctx.send(f'Joined game! Use `{ctx.prefix}mafia start` to vote to start.')

@mafia.command(name='start')
async def mafia_start(self, ctx):
    '''Vote to start the game'''
    if ctx.channel.id not in self.mafia_games or ctx.author.id not in self.mafia_games[ctx.channel.id]['players']:
        return await ctx.send('You have not joined the game.')

    game = self.mafia_games[ctx.channel.id]
    if game['started']:
        return await ctx.send('Game has already started')

    game['players'][ctx.author.id]['can_start'] = True
    yes = 0
    no = 0
    for player in game['players'].values():
        if player['can_start']: yes += 1
        else: no += 1

    if len(game['players']) < 6:
        return await ctx.send(f'Decision noted, wait for {6-len(game["players"])} more to join.')
    elif yes / no < 2:
        return await ctx.send('Decision noted, wait for more people to start.')

    mafia.start(game)
    mafia_members = ', '.join(self.bot.get_user(member) for member in game['mafia'])
    for member in game['mafia']:
        self.bot.get_user(member).send(
            'You are a member of the mafia. Each night, vote amongst your fellow mafia to kill an innocent. '
            f'These are the members of the mafia: {mafia_members}.'
        )

    self.bot.get_user(game['doctor']).send(
        'You are the doctor. Each night you can choose one person to save.'
    )
    self.bot.get_user(game['detective']).send(
        'You are the detective. Each night you can see the role of one user.'
    )
    for innocent in game['innocents']:
        self.bot.get_user(innocent).send('You are an innocent. Every day, you vote to lynch someone.')

    await mafia.prompt_voting(self.bot, game)

@mafia.command(name='vote')
async def mafia_vote(self, ctx, number: int):
    number -= 1
    if not isinstance(ctx.channel, discord.abc.PrivateChannel):
        try: await ctx.message.delete()
        except discord.Forbidden: pass
        await ctx.send('Please only vote in DMs.')

    for game in self.mafia_games.values():
        if ctx.author.id in game['players']:
            game = game
            break
    else:
        return await ctx.send('You are not in a game.')

    if not game['started']:
        return await ctx.send('The game has not started.')

    if ctx.author.id not in game['can_vote']:
        return await ctx.send('You may not vote during this phase.')

    game['votes'][ctx.author.id] = number
    await ctx.send('Vote recorded')
    if len(game['votes']) < len(game['can_vote']): return

    count = len(game['candidates'])
    mafia.process_votes(game)


    if len(game['candidates']) == 1:
        win = await mafia.next_phase(self.bot, game)
    elif count != len(game['candidates']):
        await mafia.prompt_voting(self.bot, game)
        return
    else:
        game['candidates'] = random.sample(game['candidates'], 1)
        for user in game['can_vote']:
            self.bot.get_user(user).send('Stalemate reached. Choosing random user.')

        win = await mafia.next_phase(self.bot, game)

    if win:
        del self.mafia_games[game['channel']]

@mafia_join.after_invoke
@mafia_start.after_invoke
async def save_mafia(self, _):
    yaml.dump(self.mafia_games, open('bot_data/mafia.yml', 'w'))

@commands.command()
async def spellcard(self, ctx, *search_params: lambda s: s.lower()):
    """
    Search for a touhou spellcard

    Search options:
        `--user <user>` - Find spellcards by character
        `--name <name>` - Find spellcards by name
        `--game <game>` - Find spellcards by game
        `--diff <difficulty>` - Find spellcards by difficuty
    """
    key = ''
    value = ''
    search_terms = {}
    for term in search_params:
        if term.startswith('--'):
            if value and key:
                search_terms[key] = value.strip()
                value = ''

            if term[2:] not in ['user', 'name', 'game', 'diff']:
                return await ctx.send(f'Invalid flag: `{term}`')

            key = term[2:]
        else:
            value += term + ' '

    if value and key: search_terms[key] = value.strip()
    if not search_terms:
        return await ctx.send('Unyu? Search parameters are required')

    search = self.spellcards
    for key, value in search_terms.items():
        if key == 'user':
            search = [
                spellcard for spellcard in search
                if value in spellcard['owner'].lower()
            ]
        elif key == 'name':
            search = [
                spellcard for spellcard in search
                if value in spellcard['english'].lower().replace('"', '')
            ]
        elif key == 'game':
            search = [
                spellcard for spellcard in search
                if any(
                    value == appearance['game'].lower() or
                    value in touhouwiki.GAME_ABBREVS[appearance['game']].lower()
                    for appearance in spellcard['appearances']
                )
            ]
        elif key == 'diff':
            search = [
                spellcard for spellcard in search
                if any(
                    appearance['difficulty'].lower().startswith(value) or appearance['difficulty'] == value
                    for appearance in spellcard['appearances']
                )
            ]

    if 'name' in search_terms:
        for spellcard in search:
            if search_terms['name'] == spellcard['english'].lower().replace('"', ''):
                search = [spellcard]
                break

    if len(search) == 0:
        return await ctx.send('Unyu? I didn\'t find any spellcards')
    elif len(search) > 1:
        m = '**Found the following:**\n'
        for entry in search:
            if len(m) + len(entry["owner"]) + len(entry["english"]) > 1900:
                m += '\n\nThat\'s a lot of results. I forget the rest'
                break

            m += f'\n{entry["owner"]} - {entry["english"]}'

        m += '\n\n Please narrow your search terms'
        await ctx.send(m)

    else:
        entry = search[0]
        search = entry['appearances']
        for key, value in search_terms.items():
            if key == 'game':
                search = [
                    appearance for appearance in search
                    if value == appearance['game'].lower()
                       or value in touhouwiki.GAME_ABBREVS[appearance['game']].lower()
                ]
            elif key == 'diff':
                search = [
                    appearance for appearance in search
                    if appearance['difficulty'].lower().startswith(value) or
                       appearance['difficulty'] == value
                ]

        embed = discord.Embed(title=entry['japanese'], description=entry['english'])
        embed.set_author(
            name=entry['owner'],
            url=f'https://en.touhouwiki.net/wiki/{entry["owner"].replace(" ", "_")}'
        )
        thumb = await touhouwiki.get_thumbnail(entry['owner'])
        embed.set_thumbnail(url=thumb)
        # print(search)
        if len(search) == 1:
            appearance = search[0]
            if 'others' in appearance:
                for field, value in appearance['others'].items():
                    embed.add_field(name=field or 'Notes', value=value, inline=True)

            if 'comment' in appearance and appearance['comment']:
                embed.add_field(name='Comment', value=appearance['comment'])

            try:
                embed.set_image(url=appearance['image'])
            except KeyError:
                pass

            embed.set_footer(
                text=f'{touhouwiki.GAME_ABBREVS[appearance["game"]]} | {appearance["difficulty"]}'
            )

        else:
            if 'comment' in entry and entry['comment']:
                embed.add_field(name='Comment', value=entry['comments'])

            if search:
                embed.add_field(
                    name='Appears in:',
                    value='\n'.join(f'{touhouwiki.GAME_ABBREVS[a["game"]]} - {a["difficulty"]}' for a in search),
                    inline=False
                )

        print(embed)
        await ctx.send(embed=embed)

@commands.command()
@commands.is_owner()
async def reload_spellcards(self, ctx):
    """Reload the spellcard database"""
    await ctx.send('Reloading spellcards')
    self.spellcards = await self.bot.loop.run_in_executor(
        None, touhouwiki.get_spellcards, False
    )
    await ctx.send('Reloaded spellcards')